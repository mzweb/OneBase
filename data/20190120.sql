/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : onebase

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-01-20 21:10:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `mz_action_log`
-- ----------------------------
DROP TABLE IF EXISTS `mz_action_log`;
CREATE TABLE `mz_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '执行会员id',
  `username` char(30) NOT NULL DEFAULT '' COMMENT '用户名',
  `ip` char(30) NOT NULL DEFAULT '' COMMENT '执行行为者ip',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '行为名称',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '执行的URL',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '执行行为的时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1686 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='行为日志表';

-- ----------------------------
-- Records of mz_action_log
-- ----------------------------
INSERT INTO `mz_action_log` VALUES ('1311', '1', 'admin', '127.0.0.1', '新增', '新增权限组，name：测试权限组', '/admin.php/auth/groupadd.html', '1', '1546695545', '1546695545');
INSERT INTO `mz_action_log` VALUES ('1312', '1', 'admin', '127.0.0.1', '授权', '设置权限组权限，id：18', '/admin.php/auth/menuauth.html', '1', '1546695566', '1546695566');
INSERT INTO `mz_action_log` VALUES ('1313', '1', 'admin', '127.0.0.1', '导出', '导出会员列表', '/admin.php/member/exportmemberlist.html?', '1', '1546695609', '1546695609');
INSERT INTO `mz_action_log` VALUES ('1314', '1', 'admin', '127.0.0.1', '新增', '新增会员，username：test', '/admin.php/member/memberadd.html', '1', '1546695703', '1546695703');
INSERT INTO `mz_action_log` VALUES ('1315', '967', 'test', '127.0.0.1', '登录', '登录操作，username：test', '/admin.php/login/loginhandle.html', '1', '1546695719', '1546695719');
INSERT INTO `mz_action_log` VALUES ('1316', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1546695734', '1546695734');
INSERT INTO `mz_action_log` VALUES ('1317', '1', 'admin', '127.0.0.1', '授权', '会员授权，id：967', '/admin.php/member/memberauth.html', '1', '1546695744', '1546695744');
INSERT INTO `mz_action_log` VALUES ('1318', '967', 'test', '127.0.0.1', '登录', '登录操作，username：test', '/admin.php/login/loginhandle.html', '1', '1546695756', '1546695756');
INSERT INTO `mz_action_log` VALUES ('1319', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1546695834', '1546695834');
INSERT INTO `mz_action_log` VALUES ('1320', '1', 'admin', '127.0.0.1', '编辑', '编辑会员，id：967', '/admin.php/member/memberedit.html', '1', '1546696812', '1546696812');
INSERT INTO `mz_action_log` VALUES ('1321', '1', 'admin', '127.0.0.1', '编辑', '编辑会员，id：967', '/admin.php/member/memberedit.html', '1', '1546697341', '1546697341');
INSERT INTO `mz_action_log` VALUES ('1322', '1', 'admin', '127.0.0.1', '编辑', '编辑会员，id：967', '/admin.php/member/memberedit.html', '1', '1546697359', '1546697359');
INSERT INTO `mz_action_log` VALUES ('1323', '1', 'admin', '127.0.0.1', '编辑', '编辑会员，id：967', '/admin.php/member/memberedit.html', '1', '1546697379', '1546697379');
INSERT INTO `mz_action_log` VALUES ('1324', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1546697421', '1546697421');
INSERT INTO `mz_action_log` VALUES ('1325', '1', 'admin', '127.0.0.1', '编辑', '编辑会员，id：967', '/admin.php/member/memberedit.html', '1', '1546697684', '1546697684');
INSERT INTO `mz_action_log` VALUES ('1326', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1546697717', '1546697717');
INSERT INTO `mz_action_log` VALUES ('1327', '1', 'admin', '127.0.0.1', '编辑', '编辑会员，id：967', '/admin.php/member/memberedit.html', '1', '1546697817', '1546697817');
INSERT INTO `mz_action_log` VALUES ('1328', '967', 'test', '127.0.0.1', '登录', '登录操作，username：test', '/admin.php/login/loginhandle.html', '1', '1546697840', '1546697840');
INSERT INTO `mz_action_log` VALUES ('1329', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1546698111', '1546698111');
INSERT INTO `mz_action_log` VALUES ('1330', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1546772051', '1546772051');
INSERT INTO `mz_action_log` VALUES ('1331', '1', 'admin', '127.0.0.1', '新增', 'API分组新增，name：用户接口', '/admin.php/api/apigroupadd.html', '1', '1546783111', '1546783111');
INSERT INTO `mz_action_log` VALUES ('1332', '1', 'admin', '127.0.0.1', '删除', 'API分组删除，where：id=72', '/admin.php/api/apigroupdel/id/72.html', '1', '1546783120', '1546783120');
INSERT INTO `mz_action_log` VALUES ('1333', '1', 'admin', '127.0.0.1', '恢复', '恢复回收站数据，model_name：ApiGroup，id72', '/admin.php/trash/restoredata/model_name/ApiGroup/id/72.html', '1', '1546783160', '1546783160');
INSERT INTO `mz_action_log` VALUES ('1334', '1', 'admin', '127.0.0.1', '新增', 'API新增，name：获取用户信息接口', '/admin.php/api/apiadd.html', '1', '1546783312', '1546783312');
INSERT INTO `mz_action_log` VALUES ('1335', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1546855671', '1546855671');
INSERT INTO `mz_action_log` VALUES ('1336', '1', 'admin', '127.0.0.1', '设置', '系统设置保存', '/admin.php/config/setting.html', '1', '1546855695', '1546855695');
INSERT INTO `mz_action_log` VALUES ('1337', '1', 'admin', '127.0.0.1', '设置', '系统设置保存', '/admin.php/config/setting.html', '1', '1546855701', '1546855701');
INSERT INTO `mz_action_log` VALUES ('1338', '1', 'admin', '127.0.0.1', '设置', '系统设置保存', '/admin.php/config/setting.html', '1', '1546855730', '1546855730');
INSERT INTO `mz_action_log` VALUES ('1339', '1', 'admin', '127.0.0.1', '设置', '系统设置保存', '/admin.php/config/setting.html', '1', '1546855735', '1546855735');
INSERT INTO `mz_action_log` VALUES ('1340', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547370880', '1547370880');
INSERT INTO `mz_action_log` VALUES ('1341', '1', 'admin', '127.0.0.1', '备份', '数据库备份', '/admin.php/database/databackup.html', '1', '1547370899', '1547370899');
INSERT INTO `mz_action_log` VALUES ('1342', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547431166', '1547431166');
INSERT INTO `mz_action_log` VALUES ('1343', '1', 'admin', '127.0.0.1', '新增', '新增菜单，name：模型列表', '/admin.php/menu/menuadd.html', '1', '1547433642', '1547433642');
INSERT INTO `mz_action_log` VALUES ('1344', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型列表', '/admin.php/menu/menuedit.html', '1', '1547433713', '1547433713');
INSERT INTO `mz_action_log` VALUES ('1345', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型列表', '/admin.php/menu/menuedit.html', '1', '1547433741', '1547433741');
INSERT INTO `mz_action_log` VALUES ('1346', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型列表', '/admin.php/menu/menuedit.html', '1', '1547433752', '1547433752');
INSERT INTO `mz_action_log` VALUES ('1347', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547464099', '1547464099');
INSERT INTO `mz_action_log` VALUES ('1348', '1', 'admin', '127.0.0.1', '删除', '文章分类删除，where：id=8', '/admin.php/article/articlecategorydel/id/8.html', '1', '1547464466', '1547464466');
INSERT INTO `mz_action_log` VALUES ('1349', '1', 'admin', '127.0.0.1', '恢复', '恢复回收站数据，model_name：ArticleCategory，id8', '/admin.php/trash/restoredata/model_name/ArticleCategory/id/8.html', '1', '1547464489', '1547464489');
INSERT INTO `mz_action_log` VALUES ('1350', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：1', '/admin.php/model/modelcommon.html', '1', '1547472107', '1547472107');
INSERT INTO `mz_action_log` VALUES ('1351', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：2', '/admin.php/model/modelcommon.html', '1', '1547472183', '1547472183');
INSERT INTO `mz_action_log` VALUES ('1352', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：3', '/admin.php/model/modelcommon.html', '1', '1547472213', '1547472213');
INSERT INTO `mz_action_log` VALUES ('1353', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：4', '/admin.php/model/modelcommon.html', '1', '1547474895', '1547474895');
INSERT INTO `mz_action_log` VALUES ('1354', '1', 'admin', '127.0.0.1', '编辑', '模型编辑，name：5', '/admin.php/model/modelcommon.html', '1', '1547476042', '1547476042');
INSERT INTO `mz_action_log` VALUES ('1355', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：6', '/admin.php/model/modelcommon.html', '1', '1547476142', '1547476142');
INSERT INTO `mz_action_log` VALUES ('1356', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：7', '/admin.php/model/modelcommon.html', '1', '1547476232', '1547476232');
INSERT INTO `mz_action_log` VALUES ('1357', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：4', '/admin.php/model/modelcommon.html', '1', '1547476247', '1547476247');
INSERT INTO `mz_action_log` VALUES ('1358', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：8', '/admin.php/model/modelcommon.html', '1', '1547476264', '1547476264');
INSERT INTO `mz_action_log` VALUES ('1359', '1', 'admin', '127.0.0.1', '编辑', '模型编辑，name：31', '/admin.php/model/modelcommon.html', '1', '1547476666', '1547476666');
INSERT INTO `mz_action_log` VALUES ('1360', '1', 'admin', '127.0.0.1', '编辑', '模型编辑，name：1', '/admin.php/model/modelcommon.html', '1', '1547476675', '1547476675');
INSERT INTO `mz_action_log` VALUES ('1361', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：9', '/admin.php/model/modelcommon.html', '1', '1547476683', '1547476683');
INSERT INTO `mz_action_log` VALUES ('1362', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:7', '/admin.php/model/modeldel.html', '1', '1547477807', '1547477807');
INSERT INTO `mz_action_log` VALUES ('1363', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:6', '/admin.php/model/modeldel.html', '1', '1547477902', '1547477902');
INSERT INTO `mz_action_log` VALUES ('1364', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:5', '/admin.php/model/modeldel.html', '1', '1547477938', '1547477938');
INSERT INTO `mz_action_log` VALUES ('1365', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:2', '/admin.php/model/modeldel.html', '1', '1547478011', '1547478011');
INSERT INTO `mz_action_log` VALUES ('1366', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:31', '/admin.php/model/modeldel.html', '1', '1547478035', '1547478035');
INSERT INTO `mz_action_log` VALUES ('1367', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:1', '/admin.php/model/modeldel.html', '1', '1547478076', '1547478076');
INSERT INTO `mz_action_log` VALUES ('1368', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:5', '/admin.php/model/modeldel.html', '1', '1547478149', '1547478149');
INSERT INTO `mz_action_log` VALUES ('1369', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:2', '/admin.php/model/modeldel.html', '1', '1547478224', '1547478224');
INSERT INTO `mz_action_log` VALUES ('1370', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:31', '/admin.php/model/modeldel.html', '1', '1547478229', '1547478229');
INSERT INTO `mz_action_log` VALUES ('1371', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547522490', '1547522490');
INSERT INTO `mz_action_log` VALUES ('1372', '1', 'admin', '127.0.0.1', '编辑', '编辑配置，name：trash_config', '/admin.php/config/configedit.html', '1', '1547523447', '1547523447');
INSERT INTO `mz_action_log` VALUES ('1373', '1', 'admin', '127.0.0.1', '删除', '删除回收站数据，model_name：ArticleModel，id9', '/admin.php/trash/trashdatadel/model_name/ArticleModel/id/9.html', '1', '1547523764', '1547523764');
INSERT INTO `mz_action_log` VALUES ('1374', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:8', '/admin.php/model/modeldel.html', '1', '1547523799', '1547523799');
INSERT INTO `mz_action_log` VALUES ('1375', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:4', '/admin.php/model/modeldel.html', '1', '1547523801', '1547523801');
INSERT INTO `mz_action_log` VALUES ('1376', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：10', '/admin.php/model/modelcommon.html', '1', '1547523840', '1547523840');
INSERT INTO `mz_action_log` VALUES ('1377', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:10', '/admin.php/model/modeldel.html', '1', '1547523844', '1547523844');
INSERT INTO `mz_action_log` VALUES ('1378', '1', 'admin', '127.0.0.1', '删除', '删除回收站数据，model_name：ArticleModel，id2', '/admin.php/trash/trashdatadel/model_name/ArticleModel/id/2.html', '1', '1547523865', '1547523865');
INSERT INTO `mz_action_log` VALUES ('1379', '1', 'admin', '127.0.0.1', '删除', '删除回收站数据，model_name：ArticleModel，id7', '/admin.php/trash/trashdatadel/model_name/ArticleModel/id/7.html', '1', '1547523868', '1547523868');
INSERT INTO `mz_action_log` VALUES ('1380', '1', 'admin', '127.0.0.1', '删除', '删除回收站数据，model_name：ArticleModel，id8', '/admin.php/trash/trashdatadel/model_name/ArticleModel/id/8.html', '1', '1547523870', '1547523870');
INSERT INTO `mz_action_log` VALUES ('1381', '1', 'admin', '127.0.0.1', '删除', '删除回收站数据，model_name：ArticleModel，id10', '/admin.php/trash/trashdatadel/model_name/ArticleModel/id/10.html', '1', '1547523871', '1547523871');
INSERT INTO `mz_action_log` VALUES ('1382', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:7', '/admin.php/model/modeldel.html', '1', '1547523882', '1547523882');
INSERT INTO `mz_action_log` VALUES ('1383', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:6', '/admin.php/model/modeldel.html', '1', '1547523884', '1547523884');
INSERT INTO `mz_action_log` VALUES ('1384', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:5', '/admin.php/model/modeldel.html', '1', '1547523885', '1547523885');
INSERT INTO `mz_action_log` VALUES ('1385', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:31', '/admin.php/model/modeldel.html', '1', '1547523888', '1547523888');
INSERT INTO `mz_action_log` VALUES ('1386', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:1', '/admin.php/model/modeldel.html', '1', '1547523896', '1547523896');
INSERT INTO `mz_action_log` VALUES ('1387', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：公告模型', '/admin.php/model/modelcommon.html', '1', '1547523930', '1547523930');
INSERT INTO `mz_action_log` VALUES ('1388', '1', 'admin', '127.0.0.1', '编辑', '模型编辑，name：公告', '/admin.php/model/modelcommon.html', '1', '1547523937', '1547523937');
INSERT INTO `mz_action_log` VALUES ('1389', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547547054', '1547547054');
INSERT INTO `mz_action_log` VALUES ('1390', '1', 'admin', '127.0.0.1', '新增', '新增菜单，name：模型添加', '/admin.php/menu/menuadd.html', '1', '1547553896', '1547553896');
INSERT INTO `mz_action_log` VALUES ('1391', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型添加', '/admin.php/menu/menuedit.html', '1', '1547553908', '1547553908');
INSERT INTO `mz_action_log` VALUES ('1392', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型列表', '/admin.php/menu/menuedit.html', '1', '1547554004', '1547554004');
INSERT INTO `mz_action_log` VALUES ('1393', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型添加', '/admin.php/menu/menuedit.html', '1', '1547554022', '1547554022');
INSERT INTO `mz_action_log` VALUES ('1394', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型管理', '/admin.php/menu/menuedit.html', '1', '1547554054', '1547554054');
INSERT INTO `mz_action_log` VALUES ('1395', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型管理', '/admin.php/menu/menuedit.html', '1', '1547554102', '1547554102');
INSERT INTO `mz_action_log` VALUES ('1396', '1', 'admin', '127.0.0.1', '新增', '新增菜单，name：模型列表', '/admin.php/menu/menuadd.html', '1', '1547554115', '1547554115');
INSERT INTO `mz_action_log` VALUES ('1397', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型列表', '/admin.php/menu/menuedit.html', '1', '1547554132', '1547554132');
INSERT INTO `mz_action_log` VALUES ('1398', '1', 'admin', '127.0.0.1', '数据排序', '数据排序调整，model：Menu，id：211，value：5', '/admin.php/menu/setsort.html', '1', '1547554147', '1547554147');
INSERT INTO `mz_action_log` VALUES ('1399', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型添加', '/admin.php/menu/menuedit.html', '1', '1547554185', '1547554185');
INSERT INTO `mz_action_log` VALUES ('1400', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：Menu，ids：212，status：-1', '/admin.php/menu/setstatus/ids/212/status/-1.html', '1', '1547554216', '1547554216');
INSERT INTO `mz_action_log` VALUES ('1401', '1', 'admin', '127.0.0.1', '恢复', '恢复回收站数据，model_name：Menu，id212', '/admin.php/trash/restoredata/model_name/Menu/id/212.html', '1', '1547554232', '1547554232');
INSERT INTO `mz_action_log` VALUES ('1402', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型列表', '/admin.php/menu/menuedit.html', '1', '1547554263', '1547554263');
INSERT INTO `mz_action_log` VALUES ('1403', '1', 'admin', '127.0.0.1', '新增', '新增菜单，name：字段列表', '/admin.php/menu/menuadd.html', '1', '1547554281', '1547554281');
INSERT INTO `mz_action_log` VALUES ('1404', '1', 'admin', '127.0.0.1', '新增', '新增菜单，name：字段添加', '/admin.php/menu/menuadd.html', '1', '1547554301', '1547554301');
INSERT INTO `mz_action_log` VALUES ('1405', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型管理', '/admin.php/menu/menuedit.html', '1', '1547554338', '1547554338');
INSERT INTO `mz_action_log` VALUES ('1406', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型列表', '/admin.php/menu/menuedit.html', '1', '1547554357', '1547554357');
INSERT INTO `mz_action_log` VALUES ('1407', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：字段列表', '/admin.php/menu/menuedit.html', '1', '1547554365', '1547554365');
INSERT INTO `mz_action_log` VALUES ('1408', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：字段添加', '/admin.php/menu/menuedit.html', '1', '1547554373', '1547554373');
INSERT INTO `mz_action_log` VALUES ('1409', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型列表', '/admin.php/menu/menuedit.html', '1', '1547554971', '1547554971');
INSERT INTO `mz_action_log` VALUES ('1410', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：模型添加', '/admin.php/menu/menuedit.html', '1', '1547554975', '1547554975');
INSERT INTO `mz_action_log` VALUES ('1411', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：短文', '/admin.php/model/modelcommon.html', '1', '1547554993', '1547554993');
INSERT INTO `mz_action_log` VALUES ('1412', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：摄影', '/admin.php/model/modelcommon.html', '1', '1547554999', '1547554999');
INSERT INTO `mz_action_log` VALUES ('1413', '1', 'admin', '127.0.0.1', '编辑', '编辑菜单，name：字段列表', '/admin.php/menu/menuedit.html', '1', '1547555181', '1547555181');
INSERT INTO `mz_action_log` VALUES ('1414', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：2', '/admin.php/model/fieldedit.html', '1', '1547560617', '1547560617');
INSERT INTO `mz_action_log` VALUES ('1415', '1', 'admin', '127.0.0.1', '编辑', '模型编辑，name：公告', '/admin.php/model/modelcommon.html', '1', '1547560885', '1547560885');
INSERT INTO `mz_action_log` VALUES ('1416', '1', 'admin', '127.0.0.1', '编辑', '模型编辑，name：公告', '/admin.php/model/modelcommon.html', '1', '1547560892', '1547560892');
INSERT INTO `mz_action_log` VALUES ('1417', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：3', '/admin.php/model/fieldedit.html', '1', '1547561083', '1547561083');
INSERT INTO `mz_action_log` VALUES ('1418', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：1', '/admin.php/model/fieldedit.html', '1', '1547561295', '1547561295');
INSERT INTO `mz_action_log` VALUES ('1419', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：2', '/admin.php/model/fieldedit.html', '1', '1547561837', '1547561837');
INSERT INTO `mz_action_log` VALUES ('1420', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：2', '/admin.php/model/fieldedit.html', '1', '1547561843', '1547561843');
INSERT INTO `mz_action_log` VALUES ('1421', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：2', '/admin.php/model/fieldedit.html', '1', '1547561903', '1547561903');
INSERT INTO `mz_action_log` VALUES ('1422', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：2', '/admin.php/model/fieldedit.html', '1', '1547561924', '1547561924');
INSERT INTO `mz_action_log` VALUES ('1423', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547598354', '1547598354');
INSERT INTO `mz_action_log` VALUES ('1424', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：Article，ids：32,31，status：-1', '/admin.php/article/setstatus.html', '1', '1547599125', '1547599125');
INSERT INTO `mz_action_log` VALUES ('1425', '1', 'admin', '127.0.0.1', '恢复', '恢复回收站数据，model_name：Article，id32', '/admin.php/trash/restoredata/model_name/Article/id/32.html', '1', '1547599214', '1547599214');
INSERT INTO `mz_action_log` VALUES ('1426', '1', 'admin', '127.0.0.1', '恢复', '恢复回收站数据，model_name：Article，id31', '/admin.php/trash/restoredata/model_name/Article/id/31.html', '1', '1547599216', '1547599216');
INSERT INTO `mz_action_log` VALUES ('1427', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：ArticleModelVal，ids：5，status：-1', '/admin.php/model/setstatus.html', '1', '1547599997', '1547599997');
INSERT INTO `mz_action_log` VALUES ('1428', '1', 'admin', '127.0.0.1', '设置', '系统设置保存', '/admin.php/config/setting.html', '1', '1547600557', '1547600557');
INSERT INTO `mz_action_log` VALUES ('1429', '1', 'admin', '127.0.0.1', '设置', '系统设置保存', '/admin.php/config/setting.html', '1', '1547600616', '1547600616');
INSERT INTO `mz_action_log` VALUES ('1430', '1', 'admin', '127.0.0.1', '设置', '系统设置保存', '/admin.php/config/setting.html', '1', '1547600650', '1547600650');
INSERT INTO `mz_action_log` VALUES ('1431', '1', 'admin', '127.0.0.1', '恢复', '恢复回收站数据，model_name：ArticleModelVal，val_id5', '/admin.php/trash/restoredata/model_name/ArticleModelVal/id/5.html', '1', '1547603075', '1547603075');
INSERT INTO `mz_action_log` VALUES ('1432', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：ArticleModelVal，ids：5，status：-1', '/admin.php/model/setstatus/ids/5/status/-1.html', '1', '1547603117', '1547603117');
INSERT INTO `mz_action_log` VALUES ('1433', '1', 'admin', '127.0.0.1', '删除', '删除回收站数据，model_name：ArticleModelVal，val_id5', '/admin.php/trash/trashdatadel/model_name/ArticleModelVal/id/5.html', '1', '1547603175', '1547603175');
INSERT INTO `mz_action_log` VALUES ('1434', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：1', '/admin.php/model/fieldedit.html', '1', '1547603619', '1547603619');
INSERT INTO `mz_action_log` VALUES ('1435', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：2', '/admin.php/model/fieldedit.html', '1', '1547603622', '1547603622');
INSERT INTO `mz_action_log` VALUES ('1436', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：ArticleModelVal，ids：6，status：-1', '/admin.php/model/setstatus/ids/6/status/-1.html', '1', '1547603636', '1547603636');
INSERT INTO `mz_action_log` VALUES ('1437', '1', 'admin', '127.0.0.1', '恢复', '恢复回收站数据，model_name：ArticleModelVal，val_id6', '/admin.php/trash/restoredata/model_name/ArticleModelVal/id/6.html', '1', '1547603661', '1547603661');
INSERT INTO `mz_action_log` VALUES ('1438', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：3', '/admin.php/model/fieldedit.html', '1', '1547603747', '1547603747');
INSERT INTO `mz_action_log` VALUES ('1439', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：ArticleModelVal，ids：6,7，status：0', '/admin.php/model/setstatus.html', '1', '1547603784', '1547603784');
INSERT INTO `mz_action_log` VALUES ('1440', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：ArticleModelVal，ids：6,7，status：1', '/admin.php/model/setstatus.html', '1', '1547603803', '1547603803');
INSERT INTO `mz_action_log` VALUES ('1441', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：3', '/admin.php/model/fieldedit.html', '1', '1547606139', '1547606139');
INSERT INTO `mz_action_log` VALUES ('1442', '1', 'admin', '127.0.0.1', '新增', '新增配置，name：logo', '/admin.php/config/configadd.html', '1', '1547607647', '1547607647');
INSERT INTO `mz_action_log` VALUES ('1443', '1', 'admin', '127.0.0.1', '编辑', '编辑配置，name：logo', '/admin.php/config/configedit.html', '1', '1547607663', '1547607663');
INSERT INTO `mz_action_log` VALUES ('1444', '1', 'admin', '127.0.0.1', '设置', '系统设置保存', '/admin.php/config/setting.html', '1', '1547607677', '1547607677');
INSERT INTO `mz_action_log` VALUES ('1445', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547624948', '1547624948');
INSERT INTO `mz_action_log` VALUES ('1446', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547625808', '1547625808');
INSERT INTO `mz_action_log` VALUES ('1447', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：1', '/admin.php/article/articlecategoryadd.html', '1', '1547626844', '1547626844');
INSERT INTO `mz_action_log` VALUES ('1448', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：1', '/admin.php/article/articlecategoryedit.html', '1', '1547626893', '1547626893');
INSERT INTO `mz_action_log` VALUES ('1449', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：1', '/admin.php/article/articlecategoryedit.html', '1', '1547626989', '1547626989');
INSERT INTO `mz_action_log` VALUES ('1450', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547627944', '1547627944');
INSERT INTO `mz_action_log` VALUES ('1451', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547628108', '1547628108');
INSERT INTO `mz_action_log` VALUES ('1452', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547628329', '1547628329');
INSERT INTO `mz_action_log` VALUES ('1453', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547628389', '1547628389');
INSERT INTO `mz_action_log` VALUES ('1454', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547628467', '1547628467');
INSERT INTO `mz_action_log` VALUES ('1455', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547628590', '1547628590');
INSERT INTO `mz_action_log` VALUES ('1456', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547644572', '1547644572');
INSERT INTO `mz_action_log` VALUES ('1457', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：基础', '/admin.php/article/articlecategoryedit.html', '1', '1547645258', '1547645258');
INSERT INTO `mz_action_log` VALUES ('1458', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：基础', '/admin.php/article/articlecategoryedit.html', '1', '1547645275', '1547645275');
INSERT INTO `mz_action_log` VALUES ('1459', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：基础', '/admin.php/article/articlecategoryedit.html', '1', '1547645299', '1547645299');
INSERT INTO `mz_action_log` VALUES ('1460', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：基础', '/admin.php/article/articlecategoryedit.html', '1', '1547645371', '1547645371');
INSERT INTO `mz_action_log` VALUES ('1461', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547645407', '1547645407');
INSERT INTO `mz_action_log` VALUES ('1462', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547645468', '1547645468');
INSERT INTO `mz_action_log` VALUES ('1463', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547645495', '1547645495');
INSERT INTO `mz_action_log` VALUES ('1464', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547645506', '1547645506');
INSERT INTO `mz_action_log` VALUES ('1465', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547645600', '1547645600');
INSERT INTO `mz_action_log` VALUES ('1466', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547645696', '1547645696');
INSERT INTO `mz_action_log` VALUES ('1467', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547645790', '1547645790');
INSERT INTO `mz_action_log` VALUES ('1468', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547645824', '1547645824');
INSERT INTO `mz_action_log` VALUES ('1469', '1', 'admin', '127.0.0.1', '清理', '文件清理', '/admin.php/fileclean/cleanlist.html', '1', '1547645836', '1547645836');
INSERT INTO `mz_action_log` VALUES ('1470', '1', 'admin', '127.0.0.1', '清理', '文件清理', '/admin.php/fileclean/cleanlist.html', '1', '1547645854', '1547645854');
INSERT INTO `mz_action_log` VALUES ('1471', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547645881', '1547645881');
INSERT INTO `mz_action_log` VALUES ('1472', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：4', '/admin.php/article/articlecategoryedit.html', '1', '1547645900', '1547645900');
INSERT INTO `mz_action_log` VALUES ('1473', '1', 'admin', '127.0.0.1', '删除', '文章分类删除，where：id=9', '/admin.php/article/articlecategorydel/id/9.html', '1', '1547645930', '1547645930');
INSERT INTO `mz_action_log` VALUES ('1474', '1', 'admin', '127.0.0.1', '恢复', '恢复回收站数据，model_name：ArticleCategory，id9', '/admin.php/trash/restoredata/model_name/ArticleCategory/id/9.html', '1', '1547646399', '1547646399');
INSERT INTO `mz_action_log` VALUES ('1475', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：微故事', '/admin.php/article/articlecategoryedit.html', '1', '1547646633', '1547646633');
INSERT INTO `mz_action_log` VALUES ('1476', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：1', '/admin.php/article/articlecategoryadd.html', '1', '1547647366', '1547647366');
INSERT INTO `mz_action_log` VALUES ('1477', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：2', '/admin.php/article/articlecategoryadd.html', '1', '1547650946', '1547650946');
INSERT INTO `mz_action_log` VALUES ('1478', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547688675', '1547688675');
INSERT INTO `mz_action_log` VALUES ('1479', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547690399', '1547690399');
INSERT INTO `mz_action_log` VALUES ('1480', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547690466', '1547690466');
INSERT INTO `mz_action_log` VALUES ('1481', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547690549', '1547690549');
INSERT INTO `mz_action_log` VALUES ('1482', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547690657', '1547690657');
INSERT INTO `mz_action_log` VALUES ('1483', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547690689', '1547690689');
INSERT INTO `mz_action_log` VALUES ('1484', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547691483', '1547691483');
INSERT INTO `mz_action_log` VALUES ('1485', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547691490', '1547691490');
INSERT INTO `mz_action_log` VALUES ('1486', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547691500', '1547691500');
INSERT INTO `mz_action_log` VALUES ('1487', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547691508', '1547691508');
INSERT INTO `mz_action_log` VALUES ('1488', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547691515', '1547691515');
INSERT INTO `mz_action_log` VALUES ('1489', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：2', '/admin.php/article/articlecategoryedit.html', '1', '1547691528', '1547691528');
INSERT INTO `mz_action_log` VALUES ('1490', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：后台介绍', '/admin.php/article/articlecategoryedit.html', '1', '1547691533', '1547691533');
INSERT INTO `mz_action_log` VALUES ('1491', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：基础', '/admin.php/article/articlecategoryedit.html', '1', '1547691543', '1547691543');
INSERT INTO `mz_action_log` VALUES ('1492', '1', 'admin', '127.0.0.1', '删除', '文章分类删除，where：id=7', '/admin.php/article/articlecategorydel/id/7.html', '1', '1547691852', '1547691852');
INSERT INTO `mz_action_log` VALUES ('1493', '1', 'admin', '127.0.0.1', '删除', '文章分类删除，where：id=8', '/admin.php/article/articlecategorydel/id/8.html', '1', '1547691854', '1547691854');
INSERT INTO `mz_action_log` VALUES ('1494', '1', 'admin', '127.0.0.1', '删除', '文章分类删除，where：id=9', '/admin.php/article/articlecategorydel/id/9.html', '1', '1547691856', '1547691856');
INSERT INTO `mz_action_log` VALUES ('1495', '1', 'admin', '127.0.0.1', '删除', '文章分类删除，where：id=11', '/admin.php/article/articlecategorydel/id/11.html', '1', '1547691858', '1547691858');
INSERT INTO `mz_action_log` VALUES ('1496', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：网站公告', '/admin.php/article/articlecategoryadd.html', '1', '1547691931', '1547691931');
INSERT INTO `mz_action_log` VALUES ('1497', '1', 'admin', '127.0.0.1', '删除', '删除回收站数据，model_name：ArticleCategory，id0', '/admin.php/trash/trashdatadel/model_name/ArticleCategory.html', '1', '1547692005', '1547692005');
INSERT INTO `mz_action_log` VALUES ('1498', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：微故事', '/admin.php/article/articlecategoryadd.html', '1', '1547692028', '1547692028');
INSERT INTO `mz_action_log` VALUES ('1499', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：活动', '/admin.php/article/articlecategoryadd.html', '1', '1547692081', '1547692081');
INSERT INTO `mz_action_log` VALUES ('1500', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：文学', '/admin.php/article/articlecategoryedit.html', '1', '1547692188', '1547692188');
INSERT INTO `mz_action_log` VALUES ('1501', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：文学', '/admin.php/article/articlecategoryedit.html', '1', '1547692195', '1547692195');
INSERT INTO `mz_action_log` VALUES ('1502', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：视界', '/admin.php/article/articlecategoryadd.html', '1', '1547692233', '1547692233');
INSERT INTO `mz_action_log` VALUES ('1503', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：视界', '/admin.php/article/articlecategoryedit.html', '1', '1547692238', '1547692238');
INSERT INTO `mz_action_log` VALUES ('1504', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：视界', '/admin.php/article/articlecategoryedit.html', '1', '1547692247', '1547692247');
INSERT INTO `mz_action_log` VALUES ('1505', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：微故事', '/admin.php/article/articlecategoryadd.html', '1', '1547692404', '1547692404');
INSERT INTO `mz_action_log` VALUES ('1506', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：诗词', '/admin.php/article/articlecategoryadd.html', '1', '1547692433', '1547692433');
INSERT INTO `mz_action_log` VALUES ('1507', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：心里话', '/admin.php/article/articlecategoryadd.html', '1', '1547692462', '1547692462');
INSERT INTO `mz_action_log` VALUES ('1508', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：情报', '/admin.php/article/articlecategoryedit.html', '1', '1547692508', '1547692508');
INSERT INTO `mz_action_log` VALUES ('1509', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：情报', '/admin.php/article/articlecategoryedit.html', '1', '1547692518', '1547692518');
INSERT INTO `mz_action_log` VALUES ('1510', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：头像', '/admin.php/article/articlecategoryadd.html', '1', '1547692687', '1547692687');
INSERT INTO `mz_action_log` VALUES ('1511', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：背景', '/admin.php/article/articlecategoryadd.html', '1', '1547692743', '1547692743');
INSERT INTO `mz_action_log` VALUES ('1512', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：佳人', '/admin.php/article/articlecategoryadd.html', '1', '1547692869', '1547692869');
INSERT INTO `mz_action_log` VALUES ('1513', '1', 'admin', '127.0.0.1', '新增', '文章新增，name：这是一篇测试公告', '/admin.php/article/articleadd.html', '1', '1547696192', '1547696192');
INSERT INTO `mz_action_log` VALUES ('1514', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547713162', '1547713162');
INSERT INTO `mz_action_log` VALUES ('1515', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：轮播图', '/admin.php/article/articlecategoryadd.html', '1', '1547713196', '1547713196');
INSERT INTO `mz_action_log` VALUES ('1516', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：轮播图', '/admin.php/article/articlecategoryedit.html', '1', '1547713201', '1547713201');
INSERT INTO `mz_action_log` VALUES ('1517', '1', 'admin', '127.0.0.1', '新增', '文章新增，name：轮播一', '/admin.php/article/articleadd.html', '1', '1547713638', '1547713638');
INSERT INTO `mz_action_log` VALUES ('1518', '1', 'admin', '127.0.0.1', '新增', '文章新增，name：轮播二', '/admin.php/article/articleadd.html', '1', '1547713661', '1547713661');
INSERT INTO `mz_action_log` VALUES ('1519', '1', 'admin', '127.0.0.1', '新增', '文章新增，name：轮播三', '/admin.php/article/articleadd.html', '1', '1547713675', '1547713675');
INSERT INTO `mz_action_log` VALUES ('1520', '1', 'admin', '127.0.0.1', '新增', '文章新增，name：轮播四', '/admin.php/article/articleadd.html', '1', '1547713692', '1547713692');
INSERT INTO `mz_action_log` VALUES ('1521', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：测试', '/admin.php/model/modelcommon.html', '1', '1547716236', '1547716236');
INSERT INTO `mz_action_log` VALUES ('1522', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：测试2', '/admin.php/model/modelcommon.html', '1', '1547716331', '1547716331');
INSERT INTO `mz_action_log` VALUES ('1523', '1', 'admin', '127.0.0.1', '编辑', '模型编辑，name：测试2', '/admin.php/model/modelcommon.html', '1', '1547716337', '1547716337');
INSERT INTO `mz_action_log` VALUES ('1524', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:公告', '/admin.php/model/modeldel.html', '1', '1547716355', '1547716355');
INSERT INTO `mz_action_log` VALUES ('1525', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:短文', '/admin.php/model/modeldel.html', '1', '1547716358', '1547716358');
INSERT INTO `mz_action_log` VALUES ('1526', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：1', '/admin.php/model/modelcommon.html', '1', '1547717177', '1547717177');
INSERT INTO `mz_action_log` VALUES ('1527', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:c', '/admin.php/model/modeldel.html', '1', '1547718097', '1547718097');
INSERT INTO `mz_action_log` VALUES ('1528', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:x', '/admin.php/model/modeldel.html', '1', '1547718100', '1547718100');
INSERT INTO `mz_action_log` VALUES ('1529', '1', 'admin', '127.0.0.1', '删除', '模型删除, name:z', '/admin.php/model/modeldel.html', '1', '1547718102', '1547718102');
INSERT INTO `mz_action_log` VALUES ('1530', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：d', '/admin.php/model/modelcommon.html', '1', '1547718109', '1547718109');
INSERT INTO `mz_action_log` VALUES ('1531', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：轮播图', '/admin.php/model/modelcommon.html', '1', '1547718167', '1547718167');
INSERT INTO `mz_action_log` VALUES ('1532', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：网站公告', '/admin.php/model/modelcommon.html', '1', '1547718280', '1547718280');
INSERT INTO `mz_action_log` VALUES ('1533', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：ArticleModelVal，ids：8，status：-1', '/admin.php/model/setstatus/ids/8/status/-1.html', '1', '1547725926', '1547725926');
INSERT INTO `mz_action_log` VALUES ('1534', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：5', '/admin.php/model/fieldedit.html', '1', '1547726077', '1547726077');
INSERT INTO `mz_action_log` VALUES ('1535', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：ArticleModelVal，ids：10，status：-1', '/admin.php/model/setstatus/ids/10/status/-1.html', '1', '1547726100', '1547726100');
INSERT INTO `mz_action_log` VALUES ('1536', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：ArticleModelVal，ids：9，status：-1', '/admin.php/model/setstatus/ids/9/status/-1.html', '1', '1547726102', '1547726102');
INSERT INTO `mz_action_log` VALUES ('1537', '1', 'admin', '127.0.0.1', '数据状态', '数据状态调整，model：ArticleModelVal，ids：11，status：-1', '/admin.php/model/setstatus/ids/11/status/-1.html', '1', '1547726106', '1547726106');
INSERT INTO `mz_action_log` VALUES ('1538', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：链接url', '/admin.php/model/fieldedit.html', '1', '1547729093', '1547729093');
INSERT INTO `mz_action_log` VALUES ('1539', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：1', '/admin.php/model/fieldedit.html', '1', '1547731869', '1547731869');
INSERT INTO `mz_action_log` VALUES ('1540', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：2', '/admin.php/model/fieldedit.html', '1', '1547731901', '1547731901');
INSERT INTO `mz_action_log` VALUES ('1541', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：1', '/admin.php/model/fieldedit.html', '1', '1547732074', '1547732074');
INSERT INTO `mz_action_log` VALUES ('1542', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：1', '/admin.php/model/fieldedit.html', '1', '1547732180', '1547732180');
INSERT INTO `mz_action_log` VALUES ('1543', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：1', '/admin.php/model/fieldedit.html', '1', '1547732223', '1547732223');
INSERT INTO `mz_action_log` VALUES ('1544', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：网站公告', '/admin.php/article/articlecategoryedit.html', '1', '1547733258', '1547733258');
INSERT INTO `mz_action_log` VALUES ('1545', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：网站公告', '/admin.php/article/articlecategoryedit.html', '1', '1547733432', '1547733432');
INSERT INTO `mz_action_log` VALUES ('1546', '1', 'admin', '127.0.0.1', '新增', '文章分类新增，name：头像子分类', '/admin.php/article/articlecategoryadd.html', '1', '1547735071', '1547735071');
INSERT INTO `mz_action_log` VALUES ('1547', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：头像子分类', '/admin.php/article/articlecategoryedit.html', '1', '1547735110', '1547735110');
INSERT INTO `mz_action_log` VALUES ('1548', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播三', '/admin.php/article/articleedit.html', '1', '1547735731', '1547735731');
INSERT INTO `mz_action_log` VALUES ('1549', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播二', '/admin.php/article/articleedit.html', '1', '1547735739', '1547735739');
INSERT INTO `mz_action_log` VALUES ('1550', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播三', '/admin.php/article/articleedit.html', '1', '1547735743', '1547735743');
INSERT INTO `mz_action_log` VALUES ('1551', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547735749', '1547735749');
INSERT INTO `mz_action_log` VALUES ('1552', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：文章模型', '/admin.php/model/modelcommon.html', '1', '1547736008', '1547736008');
INSERT INTO `mz_action_log` VALUES ('1553', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：作者', '/admin.php/model/fieldedit.html', '1', '1547736033', '1547736033');
INSERT INTO `mz_action_log` VALUES ('1554', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：来源', '/admin.php/model/fieldedit.html', '1', '1547736044', '1547736044');
INSERT INTO `mz_action_log` VALUES ('1555', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547775969', '1547775969');
INSERT INTO `mz_action_log` VALUES ('1556', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：文学', '/admin.php/article/articlecategoryedit.html', '1', '1547777441', '1547777441');
INSERT INTO `mz_action_log` VALUES ('1557', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：文学', '/admin.php/article/articlecategoryedit.html', '1', '1547777445', '1547777445');
INSERT INTO `mz_action_log` VALUES ('1558', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：文学', '/admin.php/article/articlecategoryedit.html', '1', '1547777449', '1547777449');
INSERT INTO `mz_action_log` VALUES ('1559', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547787284', '1547787284');
INSERT INTO `mz_action_log` VALUES ('1560', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播三', '/admin.php/article/articleedit.html', '1', '1547787800', '1547787800');
INSERT INTO `mz_action_log` VALUES ('1561', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播三', '/admin.php/article/articleedit.html', '1', '1547787805', '1547787805');
INSERT INTO `mz_action_log` VALUES ('1562', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547788887', '1547788887');
INSERT INTO `mz_action_log` VALUES ('1563', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547788895', '1547788895');
INSERT INTO `mz_action_log` VALUES ('1564', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547788904', '1547788904');
INSERT INTO `mz_action_log` VALUES ('1565', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547788957', '1547788957');
INSERT INTO `mz_action_log` VALUES ('1566', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547789156', '1547789156');
INSERT INTO `mz_action_log` VALUES ('1567', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547789162', '1547789162');
INSERT INTO `mz_action_log` VALUES ('1568', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547789199', '1547789199');
INSERT INTO `mz_action_log` VALUES ('1569', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547789211', '1547789211');
INSERT INTO `mz_action_log` VALUES ('1570', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547789355', '1547789355');
INSERT INTO `mz_action_log` VALUES ('1571', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547789362', '1547789362');
INSERT INTO `mz_action_log` VALUES ('1572', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：网站公告', '/admin.php/article/articlecategoryedit.html', '1', '1547789403', '1547789403');
INSERT INTO `mz_action_log` VALUES ('1573', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：网站公告', '/admin.php/article/articlecategoryedit.html', '1', '1547789408', '1547789408');
INSERT INTO `mz_action_log` VALUES ('1574', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：网站公告', '/admin.php/article/articlecategoryedit.html', '1', '1547789412', '1547789412');
INSERT INTO `mz_action_log` VALUES ('1575', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：网站公告', '/admin.php/article/articlecategoryedit.html', '1', '1547790824', '1547790824');
INSERT INTO `mz_action_log` VALUES ('1576', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：网站公告', '/admin.php/article/articlecategoryedit.html', '1', '1547790831', '1547790831');
INSERT INTO `mz_action_log` VALUES ('1577', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：网站公告', '/admin.php/article/articlecategoryedit.html', '1', '1547790977', '1547790977');
INSERT INTO `mz_action_log` VALUES ('1578', '1', 'admin', '127.0.0.1', '编辑', '文章分类编辑，name：网站公告', '/admin.php/article/articlecategoryedit.html', '1', '1547790998', '1547790998');
INSERT INTO `mz_action_log` VALUES ('1579', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547791091', '1547791091');
INSERT INTO `mz_action_log` VALUES ('1580', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547791100', '1547791100');
INSERT INTO `mz_action_log` VALUES ('1581', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547791108', '1547791108');
INSERT INTO `mz_action_log` VALUES ('1582', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547791179', '1547791179');
INSERT INTO `mz_action_log` VALUES ('1583', '1', 'admin', '127.0.0.1', '新增', '模型新增，name：测试模型', '/admin.php/model/modelcommon.html', '1', '1547796056', '1547796056');
INSERT INTO `mz_action_log` VALUES ('1584', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：测试普通文本', '/admin.php/model/fieldedit.html', '1', '1547796072', '1547796072');
INSERT INTO `mz_action_log` VALUES ('1585', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：测试单选', '/admin.php/model/fieldedit.html', '1', '1547796100', '1547796100');
INSERT INTO `mz_action_log` VALUES ('1586', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：测试复选', '/admin.php/model/fieldedit.html', '1', '1547796131', '1547796131');
INSERT INTO `mz_action_log` VALUES ('1587', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：测试下拉', '/admin.php/model/fieldedit.html', '1', '1547796178', '1547796178');
INSERT INTO `mz_action_log` VALUES ('1588', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：测试文本域', '/admin.php/model/fieldedit.html', '1', '1547796272', '1547796272');
INSERT INTO `mz_action_log` VALUES ('1589', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：测试时间戳', '/admin.php/model/fieldedit.html', '1', '1547796302', '1547796302');
INSERT INTO `mz_action_log` VALUES ('1590', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547802410', '1547802410');
INSERT INTO `mz_action_log` VALUES ('1591', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试单选', '/admin.php/model/fieldedit.html', '1', '1547802915', '1547802915');
INSERT INTO `mz_action_log` VALUES ('1592', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试复选', '/admin.php/model/fieldedit.html', '1', '1547802922', '1547802922');
INSERT INTO `mz_action_log` VALUES ('1593', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试下拉', '/admin.php/model/fieldedit.html', '1', '1547802944', '1547802944');
INSERT INTO `mz_action_log` VALUES ('1594', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试文本域', '/admin.php/model/fieldedit.html', '1', '1547802949', '1547802949');
INSERT INTO `mz_action_log` VALUES ('1595', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试时间戳', '/admin.php/model/fieldedit.html', '1', '1547802953', '1547802953');
INSERT INTO `mz_action_log` VALUES ('1596', '1', 'admin', '127.0.0.1', '新增', '模型字段新增，name：测试附件', '/admin.php/model/fieldedit.html', '1', '1547802969', '1547802969');
INSERT INTO `mz_action_log` VALUES ('1597', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547805399', '1547805399');
INSERT INTO `mz_action_log` VALUES ('1598', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547809996', '1547809996');
INSERT INTO `mz_action_log` VALUES ('1599', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试复选', '/admin.php/model/fieldedit.html', '1', '1547817108', '1547817108');
INSERT INTO `mz_action_log` VALUES ('1600', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试单选', '/admin.php/model/fieldedit.html', '1', '1547817114', '1547817114');
INSERT INTO `mz_action_log` VALUES ('1601', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试单选', '/admin.php/model/fieldedit.html', '1', '1547817280', '1547817280');
INSERT INTO `mz_action_log` VALUES ('1602', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试单选', '/admin.php/model/fieldedit.html', '1', '1547817647', '1547817647');
INSERT INTO `mz_action_log` VALUES ('1603', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试复选', '/admin.php/model/fieldedit.html', '1', '1547820276', '1547820276');
INSERT INTO `mz_action_log` VALUES ('1604', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试下拉', '/admin.php/model/fieldedit.html', '1', '1547820286', '1547820286');
INSERT INTO `mz_action_log` VALUES ('1605', '1', 'admin', '127.0.0.1', '编辑', '编辑会员，id：967', '/admin.php/member/memberedit.html', '1', '1547820900', '1547820900');
INSERT INTO `mz_action_log` VALUES ('1606', '1', 'admin', '127.0.0.1', '授权', '设置权限组权限，id：18', '/admin.php/auth/menuauth.html', '1', '1547821023', '1547821023');
INSERT INTO `mz_action_log` VALUES ('1607', '967', 'test', '127.0.0.1', '登录', '登录操作，username：test', '/admin.php/login/loginhandle.html', '1', '1547821048', '1547821048');
INSERT INTO `mz_action_log` VALUES ('1608', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547880241', '1547880241');
INSERT INTO `mz_action_log` VALUES ('1609', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547880692', '1547880692');
INSERT INTO `mz_action_log` VALUES ('1610', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547880716', '1547880716');
INSERT INTO `mz_action_log` VALUES ('1611', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547883876', '1547883876');
INSERT INTO `mz_action_log` VALUES ('1612', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547883896', '1547883896');
INSERT INTO `mz_action_log` VALUES ('1613', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547883965', '1547883965');
INSERT INTO `mz_action_log` VALUES ('1614', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：这是一篇测试公告1', '/admin.php/article/articleedit.html', '1', '1547884002', '1547884002');
INSERT INTO `mz_action_log` VALUES ('1615', '1', 'admin', '127.0.0.1', '新增', '文章新增，name：测试文章1', '/admin.php/article/articleadd.html', '1', '1547884042', '1547884042');
INSERT INTO `mz_action_log` VALUES ('1616', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547890289', '1547890289');
INSERT INTO `mz_action_log` VALUES ('1617', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547891454', '1547891454');
INSERT INTO `mz_action_log` VALUES ('1618', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547891490', '1547891490');
INSERT INTO `mz_action_log` VALUES ('1619', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547891575', '1547891575');
INSERT INTO `mz_action_log` VALUES ('1620', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547891727', '1547891727');
INSERT INTO `mz_action_log` VALUES ('1621', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547891741', '1547891741');
INSERT INTO `mz_action_log` VALUES ('1622', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547892091', '1547892091');
INSERT INTO `mz_action_log` VALUES ('1623', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547892107', '1547892107');
INSERT INTO `mz_action_log` VALUES ('1624', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547892294', '1547892294');
INSERT INTO `mz_action_log` VALUES ('1625', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547893482', '1547893482');
INSERT INTO `mz_action_log` VALUES ('1626', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547893512', '1547893512');
INSERT INTO `mz_action_log` VALUES ('1627', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547893698', '1547893698');
INSERT INTO `mz_action_log` VALUES ('1628', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547893745', '1547893745');
INSERT INTO `mz_action_log` VALUES ('1629', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547895042', '1547895042');
INSERT INTO `mz_action_log` VALUES ('1630', '1', 'admin', '127.0.0.1', '登录', '登录操作，username：admin', '/admin.php/login/loginhandle.html', '1', '1547947091', '1547947091');
INSERT INTO `mz_action_log` VALUES ('1631', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547947104', '1547947104');
INSERT INTO `mz_action_log` VALUES ('1632', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547947110', '1547947110');
INSERT INTO `mz_action_log` VALUES ('1633', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547947120', '1547947120');
INSERT INTO `mz_action_log` VALUES ('1634', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547947127', '1547947127');
INSERT INTO `mz_action_log` VALUES ('1635', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547947523', '1547947523');
INSERT INTO `mz_action_log` VALUES ('1636', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547947564', '1547947564');
INSERT INTO `mz_action_log` VALUES ('1637', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547947717', '1547947717');
INSERT INTO `mz_action_log` VALUES ('1638', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547947791', '1547947791');
INSERT INTO `mz_action_log` VALUES ('1639', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547948839', '1547948839');
INSERT INTO `mz_action_log` VALUES ('1640', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547948848', '1547948848');
INSERT INTO `mz_action_log` VALUES ('1641', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547948860', '1547948860');
INSERT INTO `mz_action_log` VALUES ('1642', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547948880', '1547948880');
INSERT INTO `mz_action_log` VALUES ('1643', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547948896', '1547948896');
INSERT INTO `mz_action_log` VALUES ('1644', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547950443', '1547950443');
INSERT INTO `mz_action_log` VALUES ('1645', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547950450', '1547950450');
INSERT INTO `mz_action_log` VALUES ('1646', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547950456', '1547950456');
INSERT INTO `mz_action_log` VALUES ('1647', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547950464', '1547950464');
INSERT INTO `mz_action_log` VALUES ('1648', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547950469', '1547950469');
INSERT INTO `mz_action_log` VALUES ('1649', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547950544', '1547950544');
INSERT INTO `mz_action_log` VALUES ('1650', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547950564', '1547950564');
INSERT INTO `mz_action_log` VALUES ('1651', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547950758', '1547950758');
INSERT INTO `mz_action_log` VALUES ('1652', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547950774', '1547950774');
INSERT INTO `mz_action_log` VALUES ('1653', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547950833', '1547950833');
INSERT INTO `mz_action_log` VALUES ('1654', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547951063', '1547951063');
INSERT INTO `mz_action_log` VALUES ('1655', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547951262', '1547951262');
INSERT INTO `mz_action_log` VALUES ('1656', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547951296', '1547951296');
INSERT INTO `mz_action_log` VALUES ('1657', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547951363', '1547951363');
INSERT INTO `mz_action_log` VALUES ('1658', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547951718', '1547951718');
INSERT INTO `mz_action_log` VALUES ('1659', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547951766', '1547951766');
INSERT INTO `mz_action_log` VALUES ('1660', '1', 'admin', '127.0.0.1', '编辑', '模型字段编辑，name：测试Int', '/admin.php/model/fieldedit.html', '1', '1547951871', '1547951871');
INSERT INTO `mz_action_log` VALUES ('1661', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547951939', '1547951939');
INSERT INTO `mz_action_log` VALUES ('1662', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547952166', '1547952166');
INSERT INTO `mz_action_log` VALUES ('1663', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播三', '/admin.php/article/articleedit.html', '1', '1547952184', '1547952184');
INSERT INTO `mz_action_log` VALUES ('1664', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547952220', '1547952220');
INSERT INTO `mz_action_log` VALUES ('1665', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547952244', '1547952244');
INSERT INTO `mz_action_log` VALUES ('1666', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547952311', '1547952311');
INSERT INTO `mz_action_log` VALUES ('1667', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547952328', '1547952328');
INSERT INTO `mz_action_log` VALUES ('1668', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547952346', '1547952346');
INSERT INTO `mz_action_log` VALUES ('1669', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547952368', '1547952368');
INSERT INTO `mz_action_log` VALUES ('1670', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547952385', '1547952385');
INSERT INTO `mz_action_log` VALUES ('1671', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播三', '/admin.php/article/articleedit.html', '1', '1547952477', '1547952477');
INSERT INTO `mz_action_log` VALUES ('1672', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547952575', '1547952575');
INSERT INTO `mz_action_log` VALUES ('1673', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547952600', '1547952600');
INSERT INTO `mz_action_log` VALUES ('1674', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547952637', '1547952637');
INSERT INTO `mz_action_log` VALUES ('1675', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547952766', '1547952766');
INSERT INTO `mz_action_log` VALUES ('1676', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547952920', '1547952920');
INSERT INTO `mz_action_log` VALUES ('1677', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547952946', '1547952946');
INSERT INTO `mz_action_log` VALUES ('1678', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：测试文章1', '/admin.php/article/articleedit.html', '1', '1547953017', '1547953017');
INSERT INTO `mz_action_log` VALUES ('1679', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：轮播四', '/admin.php/article/articleedit.html', '1', '1547953073', '1547953073');
INSERT INTO `mz_action_log` VALUES ('1680', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：百度链接', '/admin.php/article/articleedit.html', '1', '1547953128', '1547953128');
INSERT INTO `mz_action_log` VALUES ('1681', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：百度一下', '/admin.php/article/articleedit.html', '1', '1547953140', '1547953140');
INSERT INTO `mz_action_log` VALUES ('1682', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：明志博客', '/admin.php/article/articleedit.html', '1', '1547953150', '1547953150');
INSERT INTO `mz_action_log` VALUES ('1683', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：明志博客', '/admin.php/article/articleedit.html', '1', '1547953161', '1547953161');
INSERT INTO `mz_action_log` VALUES ('1684', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：某文章链接', '/admin.php/article/articleedit.html', '1', '1547953195', '1547953195');
INSERT INTO `mz_action_log` VALUES ('1685', '1', 'admin', '127.0.0.1', '编辑', '文章编辑，name：某栏目链接', '/admin.php/article/articleedit.html', '1', '1547953215', '1547953215');

-- ----------------------------
-- Table structure for `mz_addon`
-- ----------------------------
DROP TABLE IF EXISTS `mz_addon`;
CREATE TABLE `mz_addon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名称',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '插件描述',
  `config` text NOT NULL COMMENT '配置',
  `author` varchar(40) NOT NULL DEFAULT '' COMMENT '作者',
  `version` varchar(20) NOT NULL DEFAULT '' COMMENT '版本号',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='插件表';

-- ----------------------------
-- Records of mz_addon
-- ----------------------------
INSERT INTO `mz_addon` VALUES ('3', 'File', '文件上传', '文件上传插件', '', 'Jack', '1.0', '1', '0', '0');
INSERT INTO `mz_addon` VALUES ('4', 'Icon', '图标选择', '图标选择插件', '', 'Bigotry', '1.0', '1', '0', '0');
INSERT INTO `mz_addon` VALUES ('5', 'Editor', '文本编辑器', '富文本编辑器', '', 'Bigotry', '1.0', '1', '0', '0');

-- ----------------------------
-- Table structure for `mz_api`
-- ----------------------------
DROP TABLE IF EXISTS `mz_api`;
CREATE TABLE `mz_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(150) NOT NULL DEFAULT '' COMMENT '接口名称',
  `group_id` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '接口分组',
  `request_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '请求类型 0:POST  1:GET',
  `api_url` char(50) NOT NULL DEFAULT '' COMMENT '请求路径',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '接口描述',
  `describe_text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '接口富文本描述',
  `is_request_data` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否需要请求数据',
  `request_data` text NOT NULL COMMENT '请求数据',
  `response_data` text NOT NULL COMMENT '响应数据',
  `is_response_data` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否需要响应数据',
  `is_user_token` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否需要用户token',
  `is_response_sign` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否返回数据签名',
  `is_request_sign` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证请求数据签名',
  `response_examples` text NOT NULL COMMENT '响应栗子',
  `developer` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '研发者',
  `api_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '接口状态（0:待研发，1:研发中，2:测试中，3:已完成）',
  `is_page` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为分页接口 0：否  1：是',
  `sort` tinyint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=193 DEFAULT CHARSET=utf8 COMMENT='API表';

-- ----------------------------
-- Records of mz_api
-- ----------------------------
INSERT INTO `mz_api` VALUES ('186', '登录或注册', '34', '0', 'common/login', '系统登录注册接口，若用户名存在则验证密码正确性，若用户名不存在则注册新用户，返回 user_token 用于操作需验证身份的接口', '', '1', '[{\"field_name\":\"username\",\"data_type\":\"0\",\"is_require\":\"1\",\"field_describe\":\"\\u7528\\u6237\\u540d\"},{\"field_name\":\"password\",\"data_type\":\"0\",\"is_require\":\"1\",\"field_describe\":\"\\u5bc6\\u7801\"}]', '[{\"field_name\":\"data\",\"data_type\":\"2\",\"field_describe\":\"\\u4f1a\\u5458\\u6570\\u636e\\u53causer_token\"}]', '1', '0', '1', '0', '{\r\n    &quot;code&quot;: 0,\r\n    &quot;msg&quot;: &quot;操作成功&quot;,\r\n    &quot;data&quot;: {\r\n        &quot;member_id&quot;: 51,\r\n        &quot;nickname&quot;: &quot;sadasdas&quot;,\r\n        &quot;username&quot;: &quot;sadasdas&quot;,\r\n        &quot;create_time&quot;: &quot;2017-09-09 13:40:17&quot;,\r\n        &quot;user_token&quot;: &quot;eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmVCYXNlIEpXVCIsImlhdCI6MTUwNDkzNTYxNywiZXhwIjoxNTA0OTM2NjE3LCJhdWQiOiJPbmVCYXNlIiwic3ViIjoiT25lQmFzZSIsImRhdGEiOnsibWVtYmVyX2lkIjo1MSwibmlja25hbWUiOiJzYWRhc2RhcyIsInVzZXJuYW1lIjoic2FkYXNkYXMiLCJjcmVhdGVfdGltZSI6IjIwMTctMDktMDkgMTM6NDA6MTcifX0.6PEShODuifNsa-x1TumLoEaR2TCXpUEYgjpD3Mz3GRM&quot;\r\n    }\r\n}', '0', '1', '0', '0', '1', '1504501410', '1520504982');
INSERT INTO `mz_api` VALUES ('187', '文章分类列表', '44', '0', 'article/categorylist', '文章分类列表接口', '', '0', '', '[{\"field_name\":\"id\",\"data_type\":\"0\",\"field_describe\":\"\\u6587\\u7ae0\\u5206\\u7c7bID\"},{\"field_name\":\"name\",\"data_type\":\"0\",\"field_describe\":\"\\u6587\\u7ae0\\u5206\\u7c7b\\u540d\\u79f0\"}]', '1', '0', '0', '0', '{\r\n    &quot;code&quot;: 0,\r\n    &quot;msg&quot;: &quot;操作成功&quot;,\r\n    &quot;data&quot;: [\r\n        {\r\n            &quot;id&quot;: 2,\r\n            &quot;name&quot;: &quot;测试文章分类2&quot;\r\n        },\r\n        {\r\n            &quot;id&quot;: 1,\r\n            &quot;name&quot;: &quot;测试文章分类1&quot;\r\n        }\r\n    ]\r\n}', '0', '0', '0', '2', '1', '1504765581', '1520504982');
INSERT INTO `mz_api` VALUES ('188', '文章列表', '44', '0', 'article/articlelist', '文章列表接口', '', '1', '[{\"field_name\":\"category_id\",\"data_type\":\"0\",\"is_require\":\"0\",\"field_describe\":\"\\u82e5\\u4e0d\\u4f20\\u9012\\u6b64\\u53c2\\u6570\\u5219\\u4e3a\\u6240\\u6709\\u5206\\u7c7b\"}]', '', '0', '0', '0', '0', '{\r\n    &quot;code&quot;: 0,\r\n    &quot;msg&quot;: &quot;操作成功&quot;,\r\n    &quot;data&quot;: {\r\n        &quot;total&quot;: 9,\r\n        &quot;per_page&quot;: &quot;10&quot;,\r\n        &quot;current_page&quot;: 1,\r\n        &quot;last_page&quot;: 1,\r\n        &quot;data&quot;: [\r\n            {\r\n                &quot;id&quot;: 16,\r\n                &quot;name&quot;: &quot;11111111&quot;,\r\n                &quot;category_id&quot;: 2,\r\n                &quot;describe&quot;: &quot;22222222&quot;,\r\n                &quot;create_time&quot;: &quot;2017-08-07 13:58:37&quot;\r\n            },\r\n            {\r\n                &quot;id&quot;: 15,\r\n                &quot;name&quot;: &quot;tttttt&quot;,\r\n                &quot;category_id&quot;: 1,\r\n                &quot;describe&quot;: &quot;sddd&quot;,\r\n                &quot;create_time&quot;: &quot;2017-08-07 13:24:46&quot;\r\n            }\r\n        ]\r\n    }\r\n}', '0', '0', '1', '1', '1', '1504779780', '1520504982');
INSERT INTO `mz_api` VALUES ('189', '首页接口', '45', '0', 'combination/index', '首页聚合接口', '', '1', '[{\"field_name\":\"category_id\",\"data_type\":\"0\",\"is_require\":\"0\",\"field_describe\":\"\\u6587\\u7ae0\\u5206\\u7c7bID\"}]', '[{\"field_name\":\"article_category_list\",\"data_type\":\"2\",\"field_describe\":\"\\u6587\\u7ae0\\u5206\\u7c7b\\u6570\\u636e\"},{\"field_name\":\"article_list\",\"data_type\":\"2\",\"field_describe\":\"\\u6587\\u7ae0\\u6570\\u636e\"}]', '1', '0', '1', '0', '{\r\n    &quot;code&quot;: 0,\r\n    &quot;msg&quot;: &quot;操作成功&quot;,\r\n    &quot;data&quot;: {\r\n        &quot;article_category_list&quot;: [\r\n            {\r\n                &quot;id&quot;: 2,\r\n                &quot;name&quot;: &quot;测试文章分类2&quot;\r\n            },\r\n            {\r\n                &quot;id&quot;: 1,\r\n                &quot;name&quot;: &quot;测试文章分类1&quot;\r\n            }\r\n        ],\r\n        &quot;article_list&quot;: {\r\n            &quot;total&quot;: 8,\r\n            &quot;per_page&quot;: &quot;2&quot;,\r\n            &quot;current_page&quot;: &quot;1&quot;,\r\n            &quot;last_page&quot;: 4,\r\n            &quot;data&quot;: [\r\n                {\r\n                    &quot;id&quot;: 15,\r\n                    &quot;name&quot;: &quot;tttttt&quot;,\r\n                    &quot;category_id&quot;: 1,\r\n                    &quot;describe&quot;: &quot;sddd&quot;,\r\n                    &quot;create_time&quot;: &quot;2017-08-07 13:24:46&quot;\r\n                },\r\n                {\r\n                    &quot;id&quot;: 14,\r\n                    &quot;name&quot;: &quot;1111111111111111111&quot;,\r\n                    &quot;category_id&quot;: 1,\r\n                    &quot;describe&quot;: &quot;123123&quot;,\r\n                    &quot;create_time&quot;: &quot;2017-08-04 15:37:20&quot;\r\n                }\r\n            ]\r\n        }\r\n    }\r\n}', '0', '0', '1', '0', '1', '1504785072', '1520504982');
INSERT INTO `mz_api` VALUES ('190', '详情页接口', '45', '0', 'combination/details', '详情页接口', '', '1', '[{\"field_name\":\"article_id\",\"data_type\":\"0\",\"is_require\":\"1\",\"field_describe\":\"\\u6587\\u7ae0ID\"}]', '[{\"field_name\":\"article_category_list\",\"data_type\":\"2\",\"field_describe\":\"\\u6587\\u7ae0\\u5206\\u7c7b\\u6570\\u636e\"},{\"field_name\":\"article_details\",\"data_type\":\"2\",\"field_describe\":\"\\u6587\\u7ae0\\u8be6\\u60c5\\u6570\\u636e\"}]', '1', '0', '0', '0', '{\r\n    &quot;code&quot;: 0,\r\n    &quot;msg&quot;: &quot;操作成功&quot;,\r\n    &quot;data&quot;: {\r\n        &quot;article_category_list&quot;: [\r\n            {\r\n                &quot;id&quot;: 2,\r\n                &quot;name&quot;: &quot;测试文章分类2&quot;\r\n            },\r\n            {\r\n                &quot;id&quot;: 1,\r\n                &quot;name&quot;: &quot;测试文章分类1&quot;\r\n            }\r\n        ],\r\n        &quot;article_details&quot;: {\r\n            &quot;id&quot;: 1,\r\n            &quot;name&quot;: &quot;213&quot;,\r\n            &quot;category_id&quot;: 1,\r\n            &quot;describe&quot;: &quot;test001&quot;,\r\n            &quot;content&quot;: &quot;第三方发送到&quot;&quot;&quot;,\r\n            &quot;create_time&quot;: &quot;2014-07-22 11:56:53&quot;\r\n        }\r\n    }\r\n}', '0', '0', '0', '0', '1', '1504922092', '1520504982');
INSERT INTO `mz_api` VALUES ('191', '修改密码', '34', '0', 'common/changepassword', '修改密码接口', '', '1', '[{\"field_name\":\"old_password\",\"data_type\":\"0\",\"is_require\":\"1\",\"field_describe\":\"\\u65e7\\u5bc6\\u7801\"},{\"field_name\":\"new_password\",\"data_type\":\"0\",\"is_require\":\"1\",\"field_describe\":\"\\u65b0\\u5bc6\\u7801\"}]', '', '0', '1', '0', '0', '{\r\n    &quot;code&quot;: 0,\r\n    &quot;msg&quot;: &quot;操作成功&quot;,\r\n    &quot;exe_time&quot;: &quot;0.037002&quot;\r\n}', '0', '0', '0', '0', '1', '1504941496', '1520504982');
INSERT INTO `mz_api` VALUES ('192', '获取用户信息接口', '72', '0', 'member/getinfos', '', 'xxxxx', '1', '[{\"field_name\":\"id\",\"data_type\":\"0\",\"is_require\":\"1\",\"field_describe\":\"\\u7528\\u6237id\"}]', '[{\"field_name\":\"\\u7565\",\"data_type\":\"0\",\"field_describe\":\"\\u7565\"}]', '1', '0', '0', '0', '', '0', '2', '0', '0', '1', '1546783312', '1546783312');

-- ----------------------------
-- Table structure for `mz_api_group`
-- ----------------------------
DROP TABLE IF EXISTS `mz_api_group`;
CREATE TABLE `mz_api_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(120) NOT NULL DEFAULT '' COMMENT 'aip分组名称',
  `sort` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='api分组表';

-- ----------------------------
-- Records of mz_api_group
-- ----------------------------
INSERT INTO `mz_api_group` VALUES ('34', '基础接口', '0', '1504501195', '0', '1');
INSERT INTO `mz_api_group` VALUES ('44', '文章接口', '1', '1504765319', '1504765319', '1');
INSERT INTO `mz_api_group` VALUES ('45', '聚合接口', '0', '1504784149', '1504784149', '1');
INSERT INTO `mz_api_group` VALUES ('72', '用户接口', '0', '1546783160', '1546783111', '1');

-- ----------------------------
-- Table structure for `mz_article`
-- ----------------------------
DROP TABLE IF EXISTS `mz_article`;
CREATE TABLE `mz_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员id',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '文章名称',
  `alias` varchar(60) DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文章分类',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `content` text NOT NULL COMMENT '文章内容',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面图片id',
  `file_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件id',
  `img_ids` varchar(200) NOT NULL DEFAULT '',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `sort` tinyint(3) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `model_id` int(10) unsigned DEFAULT NULL,
  `template` varchar(60) DEFAULT NULL COMMENT '模板名称',
  `show` varchar(60) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文章表';

-- ----------------------------
-- Records of mz_article
-- ----------------------------
INSERT INTO `mz_article` VALUES ('41', '1', '这是一篇测试公告1', 'alias', '12', '这是一篇测试公告这是一篇测试公告', '这是一篇测试公告这是一篇测试公告这是一篇测试公告这是一篇测试公告这是一篇测试公告这是一篇测试公告这是一篇测试公告这是一篇测试公告', '0', '0', '', '1547696192', null, '1547884002', '23', 'template', '1', '1');
INSERT INTO `mz_article` VALUES ('42', '1', '某栏目链接', '', '22', '', '111', '10', '0', '', '1547713638', null, '1547953214', '23', '', '', '1');
INSERT INTO `mz_article` VALUES ('43', '1', '某文章链接', '', '22', '', '1', '11', '0', '', '1547713661', null, '1547953194', '23', '', '', '1');
INSERT INTO `mz_article` VALUES ('44', '1', '明志博客', '', '22', '明志博客链接', '1', '12', '0', '', '1547713675', null, '1547953161', '23', '', '', '1');
INSERT INTO `mz_article` VALUES ('45', '1', '百度一下', '', '22', '', '1', '13', '0', '', '1547713692', null, '1547953140', '23', '', '1', '1');
INSERT INTO `mz_article` VALUES ('46', '1', '测试文章1', 'test', '12', '', '1111', '0', '0', '', '1547884042', null, '1547953017', '26', '', '1', '1');

-- ----------------------------
-- Table structure for `mz_article_category`
-- ----------------------------
DROP TABLE IF EXISTS `mz_article_category`;
CREATE TABLE `mz_article_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `alias` varchar(30) DEFAULT NULL,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态',
  `child_template` varchar(60) DEFAULT NULL,
  `template` varchar(60) NOT NULL COMMENT '分类模板',
  `icon` char(20) NOT NULL DEFAULT '' COMMENT '分类图标',
  `show` varchar(60) DEFAULT '1,2' COMMENT '显示位置',
  `child_model` int(10) unsigned DEFAULT NULL,
  `thumbnail` varchar(60) DEFAULT NULL COMMENT '缩略图',
  `sort` tinyint(3) unsigned DEFAULT NULL COMMENT '排序值',
  `pid` int(10) unsigned DEFAULT NULL COMMENT '上级id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='分类表';

-- ----------------------------
-- Records of mz_article_category
-- ----------------------------
INSERT INTO `mz_article_category` VALUES ('10', '1', '1', '111', '1547647366', '1547647366', '1', 'article', 'default', '', '1,2', null, '', '50', '9');
INSERT INTO `mz_article_category` VALUES ('12', 'notice', '网站公告', '这里是网站公告', '1547691931', '1547790998', '1', 'article', 'default', '', '', '24', '20190117/9ef14c61b60f5002c3b8381f148626a3.png', '1', '0');
INSERT INTO `mz_article_category` VALUES ('13', 'letter', '文学', '', '1547692028', '1547777449', '1', 'article', 'default', '', '1,2', '23', '', '10', '0');
INSERT INTO `mz_article_category` VALUES ('14', 'activity', '活动', '', '1547692081', '1547692081', '1', 'article', 'default', '', '1,2,3', '0', '', '50', '0');
INSERT INTO `mz_article_category` VALUES ('15', 'img', '视界', '', '1547692233', '1547692247', '1', 'article', 'default', '', '1,2', '0', '', '11', '0');
INSERT INTO `mz_article_category` VALUES ('16', 'story', '微故事', '', '1547692404', '1547692404', '1', 'article', 'default', '', '1,2', '0', '', '50', '13');
INSERT INTO `mz_article_category` VALUES ('17', 'poetry', '诗词', '', '1547692433', '1547692433', '1', 'article', 'default', '', '1,2', '0', '', '50', '13');
INSERT INTO `mz_article_category` VALUES ('18', 'pass', '情报', '', '1547692462', '1547692518', '1', 'article', 'default', '', '1,2', '0', '', '50', '13');
INSERT INTO `mz_article_category` VALUES ('19', 'header', '头像', '', '1547692687', '1547692687', '1', 'article', 'default', '', '1,2', '0', '', '50', '15');
INSERT INTO `mz_article_category` VALUES ('20', 'back', '背景', '', '1547692743', '1547692743', '1', 'article', 'default', '', '1,2', '0', '', '50', '15');
INSERT INTO `mz_article_category` VALUES ('21', 'er', '佳人', '', '1547692869', '1547692869', '1', 'article', 'default', '', '1,2', '0', '', '50', '15');
INSERT INTO `mz_article_category` VALUES ('22', 'carousel', '轮播图', '', '1547713196', '1547713201', '1', 'article', 'default', '', null, '0', '', '2', '0');
INSERT INTO `mz_article_category` VALUES ('23', '111', '头像子分类', '', '1547735071', '1547735110', '1', 'article', 'default', '', '1,2', '23', '', '50', '19');

-- ----------------------------
-- Table structure for `mz_article_model`
-- ----------------------------
DROP TABLE IF EXISTS `mz_article_model`;
CREATE TABLE `mz_article_model` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL COMMENT '模型名称',
  `update_time` int(10) unsigned DEFAULT NULL,
  `create_time` int(10) unsigned DEFAULT NULL,
  `sort` smallint(5) unsigned DEFAULT '50' COMMENT '模型权重',
  `status` tinyint(3) DEFAULT '1' COMMENT '1:正常;0:回收站',
  `alias` varchar(60) DEFAULT NULL COMMENT '别名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='文章模型表';

-- ----------------------------
-- Records of mz_article_model
-- ----------------------------
INSERT INTO `mz_article_model` VALUES ('23', '轮播图', '1547718167', '1547718167', '1', '1', 'carusel');
INSERT INTO `mz_article_model` VALUES ('24', '网站公告', '1547718280', '1547718280', '2', '1', 'notice');
INSERT INTO `mz_article_model` VALUES ('25', '文章模型', '1547736008', '1547736008', '50', '1', 'text');
INSERT INTO `mz_article_model` VALUES ('26', '测试模型', '1547796055', '1547796055', '50', '1', 'test');

-- ----------------------------
-- Table structure for `mz_article_model_val`
-- ----------------------------
DROP TABLE IF EXISTS `mz_article_model_val`;
CREATE TABLE `mz_article_model_val` (
  `val_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `model_id` mediumint(9) DEFAULT NULL,
  `name_cn` varchar(60) NOT NULL COMMENT '值名称cn',
  `name_en` varchar(60) NOT NULL COMMENT '值名称en',
  `type` tinyint(3) unsigned DEFAULT NULL COMMENT '1:文本; 2:单选; 3:复选; 4:下拉; 5:文本域; 6:附件;7:时间戳',
  `required` tinyint(3) unsigned DEFAULT '0' COMMENT '0:可选; 1:必填',
  `default_val` varchar(100) NOT NULL COMMENT '默认值',
  `value` varchar(255) NOT NULL COMMENT '可选值',
  `sort` smallint(5) unsigned DEFAULT '50' COMMENT '字段值权重',
  `create_time` int(10) unsigned DEFAULT NULL,
  `status` tinyint(3) DEFAULT '1' COMMENT '1:正常;0:回收站',
  `update_time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`val_id`),
  UNIQUE KEY `unique_enname` (`name_en`),
  UNIQUE KEY `unique_cnname` (`name_cn`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='文章模型值表';

-- ----------------------------
-- Records of mz_article_model_val
-- ----------------------------
INSERT INTO `mz_article_model_val` VALUES ('13', '23', '链接url', 'href', '1', '0', '', '', '1', '1547729093', '1', '1547729093');
INSERT INTO `mz_article_model_val` VALUES ('20', '25', '作者', 'author', '1', '1', '', '', '50', '1547736033', '1', '1547736033');
INSERT INTO `mz_article_model_val` VALUES ('21', '25', '来源', 'source', '1', '1', '', '', '50', '1547736044', '1', '1547736044');
INSERT INTO `mz_article_model_val` VALUES ('22', '26', '测试普通文本', 'text', '1', '1', '', '', '50', '1547796072', '1', '1547796072');
INSERT INTO `mz_article_model_val` VALUES ('23', '26', '测试单选', 'radio', '2', '1', '值2', '值1,值2,值3', '50', '1547796100', '1', '1547817646');
INSERT INTO `mz_article_model_val` VALUES ('24', '26', '测试复选', 'checkbox', '3', '1', '值1,值3', '值1,值2,值3', '50', '1547796131', '1', '1547820276');
INSERT INTO `mz_article_model_val` VALUES ('25', '26', '测试下拉', 'select', '4', '1', '值2', '值1,值2,值3', '50', '1547796178', '1', '1547820285');
INSERT INTO `mz_article_model_val` VALUES ('26', '26', '测试文本域', 'textarea', '5', '1', '', '', '50', '1547796272', '1', '1547802949');
INSERT INTO `mz_article_model_val` VALUES ('27', '26', '测试Int', 'time', '7', '1', '', '', '50', '1547796302', '1', '1547951871');
INSERT INTO `mz_article_model_val` VALUES ('28', '26', '测试附件', 'file', '6', '1', '', '', '50', '1547802969', '1', '1547802969');

-- ----------------------------
-- Table structure for `mz_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `mz_auth_group`;
CREATE TABLE `mz_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组id,自增主键',
  `module` varchar(20) NOT NULL DEFAULT '' COMMENT '用户组所属模块',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '用户组名称',
  `describe` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户组状态：为1正常，为0禁用,-1为删除',
  `rules` varchar(1000) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id，多个规则 , 隔开',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='权限组表';

-- ----------------------------
-- Records of mz_auth_group
-- ----------------------------
INSERT INTO `mz_auth_group` VALUES ('18', '', '测试权限组', '122312', '1', '1,68,75,124,70,126,69,135,140,141,142,143,203,204,16,17,27,144,145,146,157,166,168', '1', '1547821023', '1546695545');

-- ----------------------------
-- Table structure for `mz_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `mz_auth_group_access`;
CREATE TABLE `mz_auth_group_access` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '用户组id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户组授权表';

-- ----------------------------
-- Records of mz_auth_group_access
-- ----------------------------
INSERT INTO `mz_auth_group_access` VALUES ('967', '18', '1546695744', '1546695744', '1');

-- ----------------------------
-- Table structure for `mz_blogroll`
-- ----------------------------
DROP TABLE IF EXISTS `mz_blogroll`;
CREATE TABLE `mz_blogroll` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL DEFAULT '' COMMENT '链接名称',
  `img_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '链接图片封面',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='友情链接表';

-- ----------------------------
-- Records of mz_blogroll
-- ----------------------------

-- ----------------------------
-- Table structure for `mz_config`
-- ----------------------------
DROP TABLE IF EXISTS `mz_config`;
CREATE TABLE `mz_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置标题',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分组',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置选项',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '配置说明',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `value` text NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COMMENT='配置表';

-- ----------------------------
-- Records of mz_config
-- ----------------------------
INSERT INTO `mz_config` VALUES ('1', 'seo_title', '1', '网站标题', '1', '', '网站标题前台显示标题，优先级低于SEO模块', '1378898976', '1547607677', '1', '汉网', '3');
INSERT INTO `mz_config` VALUES ('2', 'seo_description', '2', '网站描述', '1', '', '网站搜索引擎描述，优先级低于SEO模块', '1378898976', '1547607677', '1', '汉网1', '100');
INSERT INTO `mz_config` VALUES ('3', 'seo_keywords', '2', '网站关键字', '1', '', '网站搜索引擎关键字，优先级低于SEO模块', '1378898976', '1547607677', '1', '汉网', '99');
INSERT INTO `mz_config` VALUES ('9', 'config_type_list', '3', '配置类型列表', '3', '', '主要用于数据解析和页面表单的生成', '1378898976', '1547600650', '1', '0:数字\r\n1:字符\r\n2:文本\r\n3:数组\r\n4:枚举\r\n5:图片\r\n6:文件\r\n7:富文本\r\n8:单选\r\n9:多选\r\n10:日期\r\n11:时间\r\n12:颜色', '100');
INSERT INTO `mz_config` VALUES ('20', 'config_group_list', '3', '配置分组', '3', '', '配置分组', '1379228036', '1547600650', '1', '1:基础\r\n2:数据\r\n3:系统\r\n4:API', '100');
INSERT INTO `mz_config` VALUES ('25', 'list_rows', '0', '每页数据记录数', '2', '', '数据每页显示记录数', '1379503896', '1507197630', '1', '10', '10');
INSERT INTO `mz_config` VALUES ('29', 'data_backup_part_size', '0', '数据库备份卷大小', '2', '', '该值用于限制压缩后的分卷最大长度。单位：B', '1381482488', '1507197630', '1', '52428800', '7');
INSERT INTO `mz_config` VALUES ('30', 'data_backup_compress', '4', '数据库备份文件是否启用压缩', '2', '0:不压缩\r\n1:启用压缩', '压缩备份文件需要PHP环境支持gzopen,gzwrite函数', '1381713345', '1507197630', '1', '1', '9');
INSERT INTO `mz_config` VALUES ('31', 'data_backup_compress_level', '4', '数据库备份文件压缩级别', '2', '1:普通\r\n4:一般\r\n9:最高', '数据库备份文件的压缩级别，该配置在开启压缩时生效', '1381713408', '1507197630', '1', '9', '10');
INSERT INTO `mz_config` VALUES ('33', 'allow_url', '3', '不受权限验证的url', '3', '', '', '1386644047', '1547600650', '1', '0:file/pictureupload\r\n1:addon/execute', '100');
INSERT INTO `mz_config` VALUES ('43', 'empty_list_describe', '1', '数据列表为空时的描述信息', '2', '', '', '1492278127', '1507197630', '1', 'aOh! 暂时还没有数据~', '0');
INSERT INTO `mz_config` VALUES ('44', 'trash_config', '3', '回收站配置', '3', '', 'key为模型名称，值为显示列。', '1492312698', '1547600650', '1', 'Config:name\r\nAuthGroup:name\r\nMember:nickname\r\nMenu:name\r\nArticle:name\r\nArticleCategory:name\r\nAddon:name\r\nPicture:name\r\nFile:name\r\nActionLog:describe\r\nApi:name\r\nApiGroup:name\r\nBlogroll:name\r\nArticleModel:name\r\nArticleModelVal:name_cn', '0');
INSERT INTO `mz_config` VALUES ('49', 'static_domain', '1', '静态资源域名', '1', '', '若静态资源为本地资源则此项为空，若为外部资源则为存放静态资源的域名', '1502430387', '1547607677', '1', '', '0');
INSERT INTO `mz_config` VALUES ('52', 'team_developer', '3', '研发团队人员', '4', '', '', '1504236453', '1510894595', '1', '0:Bigotry\r\n1:扫地僧', '0');
INSERT INTO `mz_config` VALUES ('53', 'api_status_option', '3', 'API接口状态', '4', '', '', '1504242433', '1510894595', '1', '0:待研发\r\n1:研发中\r\n2:测试中\r\n3:已完成', '0');
INSERT INTO `mz_config` VALUES ('54', 'api_data_type_option', '3', 'API数据类型', '4', '', '', '1504328208', '1510894595', '1', '0:字符\r\n1:文本\r\n2:数组\r\n3:文件', '0');
INSERT INTO `mz_config` VALUES ('55', 'frontend_theme', '1', '前端主题', '1', '', '', '1504762360', '1547607677', '1', 'default', '0');
INSERT INTO `mz_config` VALUES ('56', 'api_domain', '1', 'API部署域名', '4', '', '', '1504779094', '1510894595', '1', 'https://demo.onebase.org', '0');
INSERT INTO `mz_config` VALUES ('57', 'api_key', '1', 'API加密KEY', '4', '', '泄露后API将存在安全隐患', '1505302112', '1510894595', '1', 'l2V|gfZp{8`;jzR~6Y1_', '0');
INSERT INTO `mz_config` VALUES ('58', 'loading_icon', '4', '页面Loading图标设置', '1', '1:图标1\r\n2:图标2\r\n3:图标3\r\n4:图标4\r\n5:图标5\r\n6:图标6\r\n7:图标7', '页面Loading图标支持7种图标切换', '1505377202', '1547607677', '1', '7', '80');
INSERT INTO `mz_config` VALUES ('59', 'sys_file_field', '3', '文件字段配置', '3', '', 'key为模型名，值为文件列名。', '1505799386', '1547600650', '1', '0_article:file_id', '0');
INSERT INTO `mz_config` VALUES ('60', 'sys_picture_field', '3', '图片字段配置', '3', '', 'key为模型名，值为图片列名。', '1506315422', '1547600650', '1', '0_article:cover_id\r\n1_article:img_ids', '0');
INSERT INTO `mz_config` VALUES ('61', 'jwt_key', '1', 'JWT加密KEY', '4', '', '', '1506748805', '1510894595', '1', 'l2V|DSFXXXgfZp{8`;FjzR~6Y1_', '0');
INSERT INTO `mz_config` VALUES ('65', 'admin_allow_ip', '3', '超级管理员登录IP', '3', '', '后台超级管理员登录IP限制，其他角色不受限。', '1510995580', '1547600650', '1', '0:27.22.112.250', '0');
INSERT INTO `mz_config` VALUES ('66', 'pjax_mode', '8', 'PJAX模式', '3', '0:否\r\n1:是', '若为PJAX模式则浏览器不会刷新，若为常规模式则为AJAX+刷新', '1512370397', '1547600650', '1', '1', '120');
INSERT INTO `mz_config` VALUES ('67', 'logo', '5', 'logo', '1', '', '', '1547607647', '1547607677', '1', '1', '0');

-- ----------------------------
-- Table structure for `mz_driver`
-- ----------------------------
DROP TABLE IF EXISTS `mz_driver`;
CREATE TABLE `mz_driver` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `service_name` varchar(40) NOT NULL DEFAULT '' COMMENT '服务标识',
  `driver_name` varchar(20) NOT NULL DEFAULT '' COMMENT '驱动标识',
  `config` text NOT NULL COMMENT '配置',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='插件表';

-- ----------------------------
-- Records of mz_driver
-- ----------------------------

-- ----------------------------
-- Table structure for `mz_file`
-- ----------------------------
DROP TABLE IF EXISTS `mz_file`;
CREATE TABLE `mz_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '保存名称',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '远程地址',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文件表';

-- ----------------------------
-- Records of mz_file
-- ----------------------------

-- ----------------------------
-- Table structure for `mz_hook`
-- ----------------------------
DROP TABLE IF EXISTS `mz_hook`;
CREATE TABLE `mz_hook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `describe` varchar(255) NOT NULL COMMENT '描述',
  `addon_list` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 ''，''分割',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='钩子表';

-- ----------------------------
-- Records of mz_hook
-- ----------------------------
INSERT INTO `mz_hook` VALUES ('36', 'File', '文件上传钩子', 'File', '1', '0', '0');
INSERT INTO `mz_hook` VALUES ('37', 'Icon', '图标选择钩子', 'Icon', '1', '0', '0');
INSERT INTO `mz_hook` VALUES ('38', 'ArticleEditor', '富文本编辑器', 'Editor', '1', '0', '0');

-- ----------------------------
-- Table structure for `mz_member`
-- ----------------------------
DROP TABLE IF EXISTS `mz_member`;
CREATE TABLE `mz_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `nickname` char(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `username` char(16) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '密码',
  `email` char(32) NOT NULL DEFAULT '' COMMENT '用户邮箱',
  `mobile` char(15) NOT NULL DEFAULT '' COMMENT '用户手机',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户状态',
  `leader_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '上级会员ID',
  `is_share_member` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否共享会员',
  `is_inside` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为后台使用者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=968 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- ----------------------------
-- Records of mz_member
-- ----------------------------
INSERT INTO `mz_member` VALUES ('1', 'admin', 'admin', '752a31d073cfb171036d1642985ed14b', 'admin@admin.com', '18555550710', '1547947091', '1546695483', '1', '0', '0', '1');
INSERT INTO `mz_member` VALUES ('967', 'test', 'test', '05b86bd7932d69db6b7d472939062548', 'admin@qq.com', '13215061596', '1547821048', '1546695703', '1', '1', '0', '1');

-- ----------------------------
-- Table structure for `mz_menu`
-- ----------------------------
DROP TABLE IF EXISTS `mz_menu`;
CREATE TABLE `mz_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文档ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `module` char(20) NOT NULL DEFAULT '' COMMENT '模块',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `is_hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  `is_shortcut` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否快捷操作',
  `icon` char(30) NOT NULL DEFAULT '' COMMENT '图标',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of mz_menu
-- ----------------------------
INSERT INTO `mz_menu` VALUES ('1', '系统首页', '0', '1', 'admin', 'index/index', '0', '0', 'fa-home', '1', '1520506753', '0');
INSERT INTO `mz_menu` VALUES ('16', '会员管理', '0', '3', 'admin', 'member/index', '0', '0', 'fa-users', '1', '1520506753', '0');
INSERT INTO `mz_menu` VALUES ('17', '会员列表', '16', '1', 'admin', 'member/memberlist', '0', '1', 'fa-list', '1', '1495272875', '0');
INSERT INTO `mz_menu` VALUES ('18', '会员添加', '16', '2', 'admin', 'member/memberadd', '0', '0', 'fa-user-plus', '1', '1520505510', '0');
INSERT INTO `mz_menu` VALUES ('27', '权限管理', '16', '3', 'admin', 'auth/grouplist', '0', '0', 'fa-key', '1', '1520505512', '0');
INSERT INTO `mz_menu` VALUES ('32', '权限组编辑', '27', '0', 'admin', 'auth/groupedit', '1', '0', '', '1', '1492002620', '0');
INSERT INTO `mz_menu` VALUES ('34', '授权', '27', '0', 'admin', 'auth_manager/group', '1', '0', '', '1', '0', '0');
INSERT INTO `mz_menu` VALUES ('35', '菜单授权', '27', '0', 'admin', 'auth/menuauth', '1', '0', '', '1', '1492095653', '0');
INSERT INTO `mz_menu` VALUES ('36', '会员授权', '27', '0', 'admin', 'auth_manager/memberaccess', '1', '0', '', '1', '0', '0');
INSERT INTO `mz_menu` VALUES ('68', '系统管理', '0', '2', 'admin', 'config/group', '0', '0', 'fa-wrench', '1', '1520506753', '0');
INSERT INTO `mz_menu` VALUES ('69', '系统设置', '68', '3', 'admin', 'config/setting', '0', '0', 'fa-cogs', '1', '1520505460', '0');
INSERT INTO `mz_menu` VALUES ('70', '配置管理', '68', '2', 'admin', 'config/index', '0', '0', 'fa-cog', '1', '1520505457', '0');
INSERT INTO `mz_menu` VALUES ('71', '配置编辑', '70', '0', 'admin', 'config/configedit', '1', '0', '', '1', '1491674180', '0');
INSERT INTO `mz_menu` VALUES ('72', '配置删除', '70', '0', 'admin', 'config/configDel', '1', '0', '', '1', '1491674201', '0');
INSERT INTO `mz_menu` VALUES ('73', '配置添加', '70', '0', 'admin', 'config/configadd', '0', '0', 'fa-plus', '1', '1491666947', '0');
INSERT INTO `mz_menu` VALUES ('75', '菜单管理', '68', '1', 'admin', 'menu/index', '0', '0', 'fa-th-large', '1', '1520505453', '0');
INSERT INTO `mz_menu` VALUES ('98', '菜单编辑', '75', '0', 'admin', 'menu/menuedit', '1', '0', '', '1', '1512459021', '0');
INSERT INTO `mz_menu` VALUES ('124', '菜单列表', '75', '0', 'admin', 'menu/menulist', '0', '1', 'fa-list', '1', '1491318271', '0');
INSERT INTO `mz_menu` VALUES ('125', '菜单添加', '75', '0', 'admin', 'menu/menuadd', '0', '0', 'fa-plus', '1', '1491318307', '0');
INSERT INTO `mz_menu` VALUES ('126', '配置列表', '70', '0', 'admin', 'config/configlist', '0', '1', 'fa-list', '1', '1491666890', '1491666890');
INSERT INTO `mz_menu` VALUES ('127', '菜单状态', '75', '0', 'admin', 'menu/setstatus', '1', '0', '', '1', '1520506673', '1491674128');
INSERT INTO `mz_menu` VALUES ('128', '权限组添加', '27', '0', 'admin', 'auth/groupadd', '1', '0', '', '1', '1492002635', '1492002635');
INSERT INTO `mz_menu` VALUES ('134', '授权', '17', '0', 'admin', 'member/memberauth', '1', '0', '', '1', '1492238568', '1492101426');
INSERT INTO `mz_menu` VALUES ('135', '回收站', '68', '4', 'admin', 'trash/trashlist', '0', '0', ' fa-recycle', '1', '1520505468', '1492311462');
INSERT INTO `mz_menu` VALUES ('136', '回收站数据', '135', '0', 'admin', 'trash/trashdatalist', '1', '0', 'fa-database', '1', '1492319477', '1492319392');
INSERT INTO `mz_menu` VALUES ('140', '服务管理', '68', '5', 'admin', 'service/servicelist', '0', '0', 'fa-server', '1', '1520505473', '1492352972');
INSERT INTO `mz_menu` VALUES ('141', '插件管理', '68', '6', 'admin', 'addon/index', '0', '0', 'fa-puzzle-piece', '1', '1520505475', '1492427605');
INSERT INTO `mz_menu` VALUES ('142', '钩子列表', '141', '0', 'admin', 'addon/hooklist', '0', '0', 'fa-anchor', '1', '1492427665', '1492427665');
INSERT INTO `mz_menu` VALUES ('143', '插件列表', '141', '0', 'admin', 'addon/addonlist', '0', '0', 'fa-list', '1', '1492428116', '1492427838');
INSERT INTO `mz_menu` VALUES ('144', '文章管理', '0', '4', 'admin', 'article/index', '0', '0', 'fa-edit', '1', '1520506753', '1492480187');
INSERT INTO `mz_menu` VALUES ('145', '文章列表', '144', '0', 'admin', 'article/articlelist', '0', '1', 'fa-list', '1', '1492480245', '1492480245');
INSERT INTO `mz_menu` VALUES ('146', '文章分类', '144', '0', 'admin', 'article/articlecategorylist', '0', '0', 'fa-list', '1', '1492480359', '1492480342');
INSERT INTO `mz_menu` VALUES ('147', '文章分类编辑', '146', '0', 'admin', 'article/articlecategoryedit', '1', '0', '', '1', '1492485294', '1492485294');
INSERT INTO `mz_menu` VALUES ('148', '分类添加', '144', '0', 'admin', 'article/articlecategoryadd', '0', '0', 'fa-plus', '1', '1492486590', '1492486576');
INSERT INTO `mz_menu` VALUES ('149', '文章添加', '144', '0', 'admin', 'article/articleadd', '0', '0', 'fa-plus', '1', '1492518453', '1492518453');
INSERT INTO `mz_menu` VALUES ('150', '文章编辑', '145', '0', 'admin', 'article/articleedit', '1', '0', '', '1', '1492879589', '1492879589');
INSERT INTO `mz_menu` VALUES ('151', '插件安装', '143', '0', 'admin', 'addon/addoninstall', '1', '0', '', '1', '1492879763', '1492879763');
INSERT INTO `mz_menu` VALUES ('152', '插件卸载', '143', '0', 'admin', 'addon/addonuninstall', '1', '0', '', '1', '1492879789', '1492879789');
INSERT INTO `mz_menu` VALUES ('153', '文章删除', '145', '0', 'admin', 'article/articledel', '1', '0', '', '1', '1492879960', '1492879960');
INSERT INTO `mz_menu` VALUES ('154', '文章分类删除', '146', '0', 'admin', 'article/articlecategorydel', '1', '0', '', '1', '1492879995', '1492879995');
INSERT INTO `mz_menu` VALUES ('156', '驱动安装', '140', '0', 'admin', 'service/driverinstall', '1', '0', '', '1', '1502267009', '1502267009');
INSERT INTO `mz_menu` VALUES ('157', '接口管理', '0', '5', 'admin', 'api/index', '0', '0', 'fa fa-book', '1', '1520506753', '1504000434');
INSERT INTO `mz_menu` VALUES ('158', '分组管理', '157', '0', 'admin', 'api/apigrouplist', '0', '0', 'fa fa-fw fa-th-list', '1', '1504000977', '1504000723');
INSERT INTO `mz_menu` VALUES ('159', '分组添加', '157', '0', 'admin', 'api/apigroupadd', '0', '0', 'fa fa-fw fa-plus', '1', '1504004646', '1504004646');
INSERT INTO `mz_menu` VALUES ('160', '分组编辑', '157', '0', 'admin', 'api/apigroupedit', '1', '0', '', '1', '1504004710', '1504004710');
INSERT INTO `mz_menu` VALUES ('161', '分组删除', '157', '0', 'admin', 'api/apigroupdel', '1', '0', '', '1', '1504004732', '1504004732');
INSERT INTO `mz_menu` VALUES ('162', '接口列表', '157', '0', 'admin', 'api/apilist', '0', '0', 'fa fa-fw fa-th-list', '1', '1504172326', '1504172326');
INSERT INTO `mz_menu` VALUES ('163', '接口添加', '157', '0', 'admin', 'api/apiadd', '0', '0', 'fa fa-fw fa-plus', '1', '1504172352', '1504172352');
INSERT INTO `mz_menu` VALUES ('164', '接口编辑', '157', '0', 'admin', 'api/apiedit', '1', '0', '', '1', '1504172414', '1504172414');
INSERT INTO `mz_menu` VALUES ('165', '接口删除', '157', '0', 'admin', 'api/apidel', '1', '0', '', '1', '1504172435', '1504172435');
INSERT INTO `mz_menu` VALUES ('166', '优化维护', '0', '6', 'admin', 'maintain/index', '0', '0', 'fa-legal', '1', '1520506753', '1505387256');
INSERT INTO `mz_menu` VALUES ('168', '数据库', '166', '0', 'admin', 'maintain/database', '0', '0', 'fa-database', '1', '1505539670', '1505539394');
INSERT INTO `mz_menu` VALUES ('169', '数据备份', '168', '0', 'admin', 'database/databackup', '0', '0', 'fa-download', '1', '1506309900', '1505539428');
INSERT INTO `mz_menu` VALUES ('170', '数据还原', '168', '0', 'admin', 'database/datarestore', '0', '0', 'fa-exchange', '1', '1506309911', '1505539492');
INSERT INTO `mz_menu` VALUES ('171', '文件清理', '166', '0', 'admin', 'fileclean/cleanlist', '0', '0', 'fa-file', '1', '1506310152', '1505788517');
INSERT INTO `mz_menu` VALUES ('174', '行为日志', '166', '0', 'admin', 'log/loglist', '0', '1', 'fa-street-view', '1', '1507201516', '1507200836');
INSERT INTO `mz_menu` VALUES ('203', '友情链接', '68', '7', 'admin', 'blogroll/index', '0', '0', 'fa-link', '1', '1520505723', '1520505717');
INSERT INTO `mz_menu` VALUES ('204', '链接列表', '203', '0', 'admin', 'blogroll/blogrolllist', '0', '0', 'fa-th', '1', '1520505777', '1520505777');
INSERT INTO `mz_menu` VALUES ('205', '链接添加', '203', '0', 'admin', 'blogroll/blogrolladd', '0', '0', 'fa-plus', '1', '1520505826', '1520505826');
INSERT INTO `mz_menu` VALUES ('206', '链接编辑', '203', '0', 'admin', 'blogroll/blogrolledit', '1', '0', 'fa-edit', '1', '1520505863', '1520505863');
INSERT INTO `mz_menu` VALUES ('207', '链接删除', '203', '0', 'admin', 'blogroll/blogrolldel', '1', '0', 'fa-minus', '1', '1520505889', '1520505889');
INSERT INTO `mz_menu` VALUES ('208', '菜单排序', '75', '0', 'admin', 'menu/setsort', '1', '0', '', '1', '1520506696', '1520506696');
INSERT INTO `mz_menu` VALUES ('209', '会员编辑', '16', '2', 'admin', 'member/memberedit', '1', '0', 'fa-edit', '1', '1520505510', '0');
INSERT INTO `mz_menu` VALUES ('210', '修改密码', '1', '2', 'admin', 'member/editpassword', '1', '0', 'fa-edit', '1', '1520505510', '0');
INSERT INTO `mz_menu` VALUES ('211', '模型管理', '0', '4', 'admin', 'javascript:;', '0', '0', 'fa-th-list', '1', '1547554338', '1547433642');
INSERT INTO `mz_menu` VALUES ('212', '模型添加', '211', '0', 'admin', 'model/modelAdd', '1', '0', 'fa-plus', '1', '1547554975', '1547553896');
INSERT INTO `mz_menu` VALUES ('213', '模型列表', '211', '0', 'admin', 'model/modeLlist', '0', '0', 'fa-th-list', '1', '1547554971', '1547554115');
INSERT INTO `mz_menu` VALUES ('214', '字段列表', '211', '1', 'admin', 'model/fieldList', '1', '0', 'fa-th-list', '1', '1547555181', '1547554281');
INSERT INTO `mz_menu` VALUES ('215', '字段添加', '211', '1', 'admin', 'model/fieldAdd', '0', '0', 'fa-plus', '1', '1547554373', '1547554301');

-- ----------------------------
-- Table structure for `mz_model_carusel`
-- ----------------------------
DROP TABLE IF EXISTS `mz_model_carusel`;
CREATE TABLE `mz_model_carusel` (
  `aid` int(10) unsigned NOT NULL,
  `href` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mz_model_carusel
-- ----------------------------
INSERT INTO `mz_model_carusel` VALUES ('42', 'xxxxxxx');
INSERT INTO `mz_model_carusel` VALUES ('43', 'https://www.onebase.com');
INSERT INTO `mz_model_carusel` VALUES ('44', 'https://www.mzblogg.com');
INSERT INTO `mz_model_carusel` VALUES ('45', 'http://www.baidu.com');

-- ----------------------------
-- Table structure for `mz_model_notice`
-- ----------------------------
DROP TABLE IF EXISTS `mz_model_notice`;
CREATE TABLE `mz_model_notice` (
  `aid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='栏目模型';

-- ----------------------------
-- Records of mz_model_notice
-- ----------------------------

-- ----------------------------
-- Table structure for `mz_model_test`
-- ----------------------------
DROP TABLE IF EXISTS `mz_model_test`;
CREATE TABLE `mz_model_test` (
  `aid` int(10) unsigned NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  `radio` varchar(255) DEFAULT NULL,
  `checkbox` varchar(255) DEFAULT NULL,
  `select` varchar(255) DEFAULT NULL,
  `textarea` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `file` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='栏目模型';

-- ----------------------------
-- Records of mz_model_test
-- ----------------------------
INSERT INTO `mz_model_test` VALUES ('46', '222', '0', '', '1', '1111', '111', null);

-- ----------------------------
-- Table structure for `mz_model_text`
-- ----------------------------
DROP TABLE IF EXISTS `mz_model_text`;
CREATE TABLE `mz_model_text` (
  `aid` int(10) unsigned NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='栏目模型';

-- ----------------------------
-- Records of mz_model_text
-- ----------------------------

-- ----------------------------
-- Table structure for `mz_picture`
-- ----------------------------
DROP TABLE IF EXISTS `mz_picture`;
CREATE TABLE `mz_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '图片名称',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '图片链接',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='图片表';

-- ----------------------------
-- Records of mz_picture
-- ----------------------------
INSERT INTO `mz_picture` VALUES ('9', '78268cfab5363a0b6a46aafa72320158.jpg', '20190116/78268cfab5363a0b6a46aafa72320158.jpg', '', '221fe8df20ab90fe832cb18de156d25ed7db8e24', '1547645880', '1547645880', '1');
INSERT INTO `mz_picture` VALUES ('10', '6e58693e237f8768fd4ee45503b7b1ff.jpg', '20190117/6e58693e237f8768fd4ee45503b7b1ff.jpg', '', '8a6b0128f97244780a85d9b114dccb4d3a837d62', '1547713627', '1547713627', '1');
INSERT INTO `mz_picture` VALUES ('11', '38e35b3b7fb11a69b198db8435c9242d.jpg', '20190117/38e35b3b7fb11a69b198db8435c9242d.jpg', '', '0df5c49cba42f9b7e0ee70f789707090d431c4e7', '1547713659', '1547713659', '1');
INSERT INTO `mz_picture` VALUES ('12', '2af04615e6167bdaf646778e840e30be.jpg', '20190117/2af04615e6167bdaf646778e840e30be.jpg', '', '4c37fd56a0097ad64cc1a0f3aea4d390aff61784', '1547713672', '1547713672', '1');
INSERT INTO `mz_picture` VALUES ('13', '3341950d272b2af843bd94d888f85ee5.jpg', '20190117/3341950d272b2af843bd94d888f85ee5.jpg', '', '81e866ec7cde63a0482e3f1027eb62203ea32f0c', '1547713691', '1547713691', '1');
INSERT INTO `mz_picture` VALUES ('14', '9ef14c61b60f5002c3b8381f148626a3.png', '20190117/9ef14c61b60f5002c3b8381f148626a3.png', '', '61a6351cd139f11acdc95b768d83d62031a9f3eb', '1547733431', '1547733431', '1');
