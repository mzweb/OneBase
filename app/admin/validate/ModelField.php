<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\admin\validate;

/**
 * 模型验证器
 */
class ModelField extends AdminBase
{
    
    // 验证规则
    protected $rule =   [
        'model_id'          => 'require',
        'name_cn'           => 'require|unique:article_model_val',
        'name_en'           => 'require|unique:article_model_val',
        'required'          => 'require',
        'type'              => 'require',
    ];

    // 验证提示
    protected $message  =   [
        'model_id.require'       => '模型id不能为空',
        'name_cn.require'        => '模型中文名称不能为空',
        'name_en.require'        => '模型英文名称不能为空',
        'required.require'       => '是否必填不能为空',
        'type.require'           => '类型不能为空',
        'name_cn.unique'         => '模型中文名称不能重复',
        'name_en.unique'         => '模型英文名称不能重复',
    ];
    
    // 应用场景
    protected $scene = [
        'edit'  =>  ['model_id','name_cn','name_en','required','type'],
    ];
}
