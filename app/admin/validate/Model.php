<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\admin\validate;

/**
 * 模型验证器
 */
class Model extends AdminBase
{
    
    // 验证规则
    protected $rule =   [
        'name'          => 'require|unique:article_model',
    ];

    // 验证提示
    protected $message  =   [
        'name.require'         => '模型不能为空',
        'name.unique'         => '模型名称不能重复',
    ];
    
    // 应用场景
    protected $scene = [
        'edit'  =>  ['name'],
    ];
}
