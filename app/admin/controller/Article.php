<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\admin\controller;
use think\Loader;
/**
 * 文章控制器
 */
class Article extends AdminBase
{

    /**
     * 文章列表
     */
    public function articleList()
    {
        
        $where = $this->logicArticle->getWhere($this->param);

        $this->assign('list', $this->logicArticle->getArticleList($where, 'a.*,m.nickname,c.name as category_name', 'a.create_time desc'));
        
        return $this->fetch('article_list');
    }
    
    /**
     * 文章添加
     */
    public function articleAdd()
    {
        
        $this->articleCommon();
        
        return $this->fetch('article_edit');
    }
    
    /**
     * 文章编辑
     */
    public function articleEdit()
    {
        $this->articleCommon();

        $info = $this->logicArticle->getArticleInfo(['a.id' => $this->param['id']], 'a.*,m.nickname,c.name as category_name');

        !empty($info) && $info['img_ids_array'] = str2arr($info['img_ids']);

        $this->assign([
            'info'=> $info,
        ]);
        
        return $this->fetch('article_edit');
    }
    
    /**
     * 文章添加与编辑通用方法
     */
    public function articleCommon()
    {
        IS_POST && $this->jump($this->logicArticle->articleEdit($this->param));

        $this->Common('article_category_list');

        $this->assign('config',config('article_type'));
    }
    
    /**
     * 文章分类添加
     */
    public function articleCategoryAdd()
    {
        IS_POST && $this->jump($this->logicArticle->articleCategoryEdit($this->param));

        $this->CategoryCommon();
        
        return $this->fetch('article_category_edit');
    }

    /**
     * 分类与文章的公用方法
     */
    public function Common($cate_name = 'list'){
        $list = $this->logicArticle->getArticleCategoryList([],true,'sort');
        $list = $this->logicCommon->TreeAlias($this->logicCommon->ListTree($list));  //分类信息树形结构

        $model_list = $this->logicModel->getModelList();    //模型信息

        $this->assign([
            $cate_name=>$list,
            'model_list'=>$model_list,
        ]);
    }

    /**
     * 分类添加/编辑/ 公用方法
     */
    public function CategoryCommon(){

        $this->Common();

        $this->assign('config',config('category_type'));
    }
    
    /**
     * 文章分类编辑
     */
    public function articleCategoryEdit()
    {
        
        IS_POST && $this->jump($this->logicArticle->articleCategoryEdit($this->param));

        $this->CategoryCommon();
        
        $info = $this->logicArticle->getArticleCategoryInfo(['id' => $this->param['id']]);

        $this->assign([
            'info'=> $info,
            'attr_type'=> config('attr_type'),  //分类属性信息
        ]);
        
        return $this->fetch('article_category_edit');
    }
    
    /**
     * 文章分类列表
     */
    public function articleCategoryList()
    {
        
        $this->CategoryCommon();
       
        return $this->fetch('article_category_list');
    }
    
    /**
     * 文章分类删除
     */
    public function articleCategoryDel($id = 0)
    {
        
        $this->jump($this->logicArticle->articleCategoryDel(['id' => $id]));
    }
    
    /**
     * 数据状态设置
     */
    public function setStatus()
    {
        
        $this->jump($this->logicAdminBase->setStatus('Article', $this->param));
    }
}
