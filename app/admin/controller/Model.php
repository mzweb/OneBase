<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\admin\controller;
use think\Loader;
/**
 * 模型控制器
 */
class Model extends AdminBase
{
    
    /**
     * 模型列表
     */
    public function modelList(){

        $this->assign('list', $this->logicModel->getModelList($this->param));
        
        return view();
    }

    /**
     * 模型修改与添加公用方法
     */
    public function modelCommon(){

        IS_POST && $this->jump($this->logicModel->modelEdit($this->param));
        
        // $this->assign('article_category_list', $this->logicArticle->getArticleCategoryList([], 'id,name', '', false));
    }

    /**
     * 模型删除
     */
    public function modelDel(){
        IS_POST && $this->jump($this->logicModel->modelDel($this->param));
    }


    /**
     * 查询现有字段数据(文章发布时ajax请求)
     * param = [
     *     'model_id','article_id'
     * ];
     */
    public function getFieldValue(){

        $table_name = Loader::model('Model','model','','common')->getTable($this->param['model_id']);

        $field_info = db($table_name)->where(['aid'=>$this->param['article_id']])->find();

        return $field_info;
    }

    /**
     * 字段列表
     */
    public function fieldList(){

        $list = $this->logicModel->getFieldList(
            input('model_id'),['name_cn|name_en'=>['like','%'.input('search_data').'%','or']]
        );
            

        $model_info = $this->logicModel->getModelInfo(input('model_id'));

        $this->assign([
            'list'=>$list,
            'model_id'=>input('model_id'),
            'model_info'=>$model_info,
        ]);

        return view();
    }

    /**
     * 获取字段列表(文章添加与发布时的 ajax获取)
     * @return [type] [description]
     */
    public function getFieldList(){
        return  $this->logicModel->getFieldList(input('model_id'));
    }


    /**
     * 字段修改与新增公用修改方法
     */
    public function fieldEdit(){
        
        IS_POST && $this->jump($this->logicModel->modelFieldEdit($this->param));
        
        // $this->assign('article_category_list', $this->logicArticle->getArticleCategoryList([], 'id,name', '', false));
    }

    /**
     * 字段新增
     * @return [type] [description]
     */
    public function fieldAdd(){

        if(input('val_id')){
            $info = $this->logicModel->getFieldInfo(input('val_id'));
            $this->assign('info',$info);
        }

        $model_info = $this->logicModel->getModelList([]);

        $this->assign([
            'model_info'=>  $model_info,
            'model_id'=>    input('model_id'),
            'attr_type'=> config('attr_type'),  //分类属性信息
        ]);

        return view();
    }


    /**
     * 数据状态设置
     */
    public function setStatus(){
        $this->jump($this->logicModel->delField($this->param));   //字段直接删除
        // $this->jump($this->logicAdminBase->setStatus('ArticleModelVal', $this->param,'val_id'));
    }






    
    /**
     * 文章添加
     */
    public function articleAdd()
    {
        
        $this->articleCommon();
        
        return $this->fetch('article_edit');
    }
    
    /**
     * 文章编辑
     */
    public function articleEdit()
    {
        
        $this->articleCommon();
        
        $info = $this->logicArticle->getArticleInfo(['a.id' => $this->param['id']], 'a.*,m.nickname,c.name as category_name');
        
        !empty($info) && $info['img_ids_array'] = str2arr($info['img_ids']);
        
        $this->assign('info', $info);
        
        return $this->fetch('article_edit');
    }
    
    /**
     * 文章添加与编辑通用方法
     */
    public function articleCommon()
    {
        
        IS_POST && $this->jump($this->logicArticle->articleEdit($this->param));
        
        $this->assign('article_category_list', $this->logicArticle->getArticleCategoryList([], 'id,name', '', false));
    }
    
    /**
     * 文章分类添加
     */
    public function articleCategoryAdd()
    {
        
        IS_POST && $this->jump($this->logicArticle->articleCategoryEdit($this->param));
        
        return $this->fetch('article_category_edit');
    }
    
    /**
     * 文章分类编辑
     */
    public function articleCategoryEdit()
    {
        
        IS_POST && $this->jump($this->logicArticle->articleCategoryEdit($this->param));
        
        $info = $this->logicArticle->getArticleCategoryInfo(['id' => $this->param['id']]);



        $this->assign([
            'info'=> $info,
            'attr_type'=> config('attr_type'),  //分类属性信息
        ]);
        
        return $this->fetch('article_category_edit');
    }
    
    /**
     * 文章分类列表
     */
    public function articleCategoryList()
    {
        
        $this->assign('list', $this->logicArticle->getArticleCategoryList());
       
        return $this->fetch('article_category_list');
    }
    
    /**
     * 文章分类删除
     */
    public function articleCategoryDel($id = 0)
    {
        
        $this->jump($this->logicArticle->articleCategoryDel(['id' => $id]));
    }
    

}
