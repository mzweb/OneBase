<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\index\controller;

/**
 * 测试接口控制器
 */
class Api extends IndexBase{

    private static $host = 'http://www.onebase.com/';

    private static $url = '';

    private static $data = [];

    public function __construct(){
        parent::__construct();
        self::$url = self::$host . self::$url;
    }

    public function __destruct(){

        if(strpos(self::$host,'http') === false){
            self::$host = 'http://' . self::$host;
        }

        $param = explode('/',self::$url);

        $param['0'] .= '.php';

        $param = implode('/',$param);

        // print_r(self::$host . $param); die;
        // dd(self::$data);
        
        $info = Curl(self::$host . $param,self::$data);

        print_r($info);
    }

    /**
     * 获取用户信息接口
     */
    public function memberGetInfo(){
        self::$data = [
            'access_token' =>  '5b7f60aca7e7f6f8c680b1b219ad3ec6',
            'id'    =>  '1',
        ];
        self::$url = 'api/member/getInfos';
        // dd($data);
    }

    

}
