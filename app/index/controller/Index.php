<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\index\controller;
use think\Loader;

/**
 * 前端首页控制器
 */
class Index extends IndexBase
{
    
    // 首页
    public function index(){
        
        // $where = [];
        
        // !empty((int)$cid) && $where['a.category_id'] = $cid;
        
        // $this->assign('article_list', $this->logicArticle->getArticleList($where, 'a.*,m.nickname,c.name as category_name', 'a.create_time desc'));
        
        // $this->assign('category_list', $this->logicArticle->getArticleCategoryList([], true, 'create_time asc', false));
        $limit = 20;
        $continue = 0;

        
        // $this->logicArticle->getCategoryList(['alias'=>'notice','continue'=>$continue,'limit'=>$limit]);
        $info = $this->logicIndex->getInit();

        $this->assign('info',$info);

        return $this->fetch('index');
    }
    
    // 详情
    public function details($id = 0)
    {
        
        $where = [];
        
        !empty((int)$id) && $where['a.id'] = $id;
        
        $data = $this->logicArticle->getArticleInfo($where);
        
        $this->assign('article_info', $data);
        
        $this->assign('category_list', $this->logicArticle->getArticleCategoryList([], true, 'create_time asc', false));
        
        return $this->fetch('details');
    }

    /**
     * api接口测试页面
     * @return [type] [description]
     */
    public function api(){
        return view();
    }

}
