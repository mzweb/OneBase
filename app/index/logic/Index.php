<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\index\logic;

use app\index\logic\IndexBase;
use think\Loader;

/**
 * Index基础逻辑
 */
class Index extends IndexBase{

	/**
	 * 首页初始化数据
	 */
	public function getInit(){
         
        $info['notice'] = $this->getCategoryArticleList(['a.alias'=>'notice'],0,5);  //五条公告数据

        // dd($info['notice']);

        $info['article'] = $this->indexArticle([]);	//推荐文章数据

        $info['carousel']	=	$this->getCarousel();	//轮播图数据

        $info['category']	=	$this->getReCategory();	//首页栏目数据

        $info['bottom_nav']	=	$this->getBottomNav();	//底部导航数据

        return $info;
	}



	/**
	 * 首页推荐栏目数据
	 */
	public function getReCategory(){
		return [];
	}

	/**
	 * 底部导航数据
	 */
	public function getBottomNav(){
		return [];
	}


	/**
	 * 获取轮播
	 */
	public function getCarousel($limit = '5'){
		$id = $this->aliasGetId('carousel');
        $list = Loader::model('Article','model','','common')->getLists([
            'a.category_id'=>$id
        ]);
        return $list;
	}

    /**
     * 获取文章
     */
    public function indexArticle($limit = '15'){
        $id = $this->aliasGetId('letter');
        $list = Loader::model('Article','model','','common')->getLists([
            'a.category_id'=>$id
        ]);
        return $list;
    }




    /**
     * 获取分类下文章列表方法
     */
    public function getCategoryArticleList($where = [],$continue='0',$limit='15'){

        if($where['a.alias']){  //将栏目别名转化为栏目id条件
            $where['a.category_id'] = $this->aliasGetId($where['a.alias']);
            unset($where['a.alias']);
        }

        $where = array_merge($where,[
            'a.status'=>1,
        ]);

        $list = db('article')   //读取文章基本信息
        ->field('a.*,m.alias as m_alias,m.status as m_status')
        ->alias('a')
        ->join('article_model m','a.model_id = m.id')
        ->where($where)
        ->order('sort')
        ->limit($continue,$limit)
        ->select();

        foreach($list as $k=>$v){   //获取附加表字段
            if($v['m_status'] == 1){
                $table_name = 'model_' . $v['m_alias'];
                $row = db($table_name)->where(['aid'=>$v['id']])->find();
                if(is_array($row)){
                    foreach($row as $key=>$val){
                        if($key != 'aid'){ $list[$k][$key] = $val; }
                    }
                }
            }else{
                unset($list[$k]);
            }
        }

        return $list;
    }
}
