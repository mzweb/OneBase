<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\index\logic;

use app\common\logic\LogicBase;

/**
 * Index基础逻辑
 */
class IndexBase extends LogicBase{
	/**
     * 根据栏目别名获取栏目id
     */
    public function aliasGetId($alias){
        $id = db('article_category')
        ->where(['status'=>1,'alias'=>$alias])
        ->value('id');
        return $id;
    }
}
