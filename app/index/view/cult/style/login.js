function eye_switch(e){
	if(e.classList.contains("eye") == true){
		$(e).removeClass("eye");
		$(e).addClass("eyes");
		$(e).parent().find("input").attr("type","text");
	}else{
		$(e).removeClass("eyes");
		$(e).addClass("eye");
		$(e).parent().find("input").attr("type","password");
	}
	
}

$(".cancel_icon").click(function(e){
	$(this).parent().find("input").val("");
});