window.onload = function(){
	navEffect();	//导航
	carouselEffect();	//轮播初始化
	manEffect();	//主体内容

}



function navEffect(){	//导航
	var banner = document.querySelector(".top"); //导航对象
	var bannerHeight = banner.offsetHeight; //获取轮播图的高度

	window.onscroll=function(){
		var offetTop = document.body.scrollTop;	//获取与顶部的距离
		if(offetTop == 0){	//兼容部分机型
			offetTop = document.documentElement.scrollTop;
		}

		var opacity = 0;	//设置初始透明度

		if(offetTop < bannerHeight || opacity == 0){
			opacity = offetTop/bannerHeight;	//透明度

			banner.style.backgroundColor = "rgba(219,219,167,"+opacity+")"; //设置导航透明度
		}

	}
}

/**
 * 轮播图初始化
 */
function carouselEffect(){
	var carousel = document.querySelector(".carousel>div");
	

	//添加首尾图片
	var first = carousel.querySelector("img:first-of-type");
	var last = carousel.querySelector("img:last-of-type");
	carousel.appendChild(first.cloneNode(true));
	carousel.insertBefore(last.cloneNode(true),first);

	//设置轮播框架长度
	var list = document.querySelectorAll(".carousel>div>img");
	var count = list.length;
	var width = count * 100;	//轮播容器长度
	var widthChild = 100 / count;	//每个轮播的长度

	//填充
	carousel.style = "width:" + width + "%;" + "left:"+-100+"%;";
	for(var i = 0; i < count; i++){
		list[i].style = "width:" + widthChild + "%";
	}

	index = 1;
	var moveEnd = false; //防止用户滑动过快


	var IntervalId = startCarousel();

	//记录初始值 
	carousel.addEventListener("touchstart",function(e){
		initX = e.changedTouches[0].clientX;	//手指初始的距离
		window.clearInterval(IntervalId);	//结束轮播
		IntervalStop = 1;
	});

	//图片随手指偏移
	carousel.addEventListener("touchmove",function(e){
		if(moveEnd == true){
			return;
		}

		var allWidth = carousel.offsetWidth;	//总宽度
		var eachWidth = allWidth / count;	//单个的宽度
		toWidth = (index) * eachWidth;	//现在的位置

		touthX = e.changedTouches[0].clientX;	//当前手指的距离
		moveX = initX - touthX;	//偏移量

		carousel.style = "width:" + width + "%;" + 
		"left:"+ -(toWidth+moveX) +"px;";
	});

	//结束滑动
	carousel.addEventListener("touchend",function(){
		if(typeof moveX == "undefined"){	
			//如果用户只是点击没有移动的话
			//防止误运行
			if(typeof IntervalStop != "undefined" && IntervalStop == 1){
				//重新开启定时器
				IntervalId = startCarousel();
				IntervalStop = 0;
			}
			return;
		}
		moveEnd = true;

		if(Math.abs(moveX) > 50){	//翻页
			if(moveX > 0){
				index++;
			}else{
				index--;
			}
		}
		else if(Math.abs(moveX)>0){	//回弹

		}

		carousel.style = "width:" + width + "%;" + 
		"left:"+-100 * index +"%;"+
		"transition:left 0.5s ease-in-out ";
		IntervalId = startCarousel();
		IntervalStop = 1;
		//重置数值
		moveX = 0;
		initX = 0;
	});

	//过渡效果结束时触发 手指滑动轮播图 最后一张和第一张的处理 
	carousel.addEventListener("webkitTransitionEnd",function(){

		setTimeout(function(){	//让用户可以重新滑动轮播图
			moveEnd = false;
			// clearInterval(IntervalId);
			// IntervalId = startCarousel();
		},100);

		if(index == count-1){
			index = 1
		}
		else if(index == 0){
			index = count-2;
		}else{
			setIndex(index);	//点标记设置
			return;
		}

		setIndex(index);	//点标记设置

		carousel.style = "width:" + width + "%;" + 
		"left:"+ -100 * index +"%;";
	});




	function startCarousel(){	//轮播定时器
		return setInterval(function(){	//轮播
			index++;
			carousel.style.transition="left 1s ease-in-out";
			carousel.style = "width:" + width + "%;" + 
			"left:"+-100 * index +"%;"+
			"transition:left 0.5s ease-in-out ";

			setTimeout(function(){	//延迟重置
				if(index == count -1){
					index = 1;
					carousel.style = "width:" + width + "%;" + 
					"left:"+ -100 * index +"%;";
					return;
				}
			},800);
		},4000);
	}

	//点标记修改
	function setIndex(index){
		var indexList = document.querySelectorAll(".carousel>ul>li");
		for(var i = 0; i<indexList.length; i++){
			indexList[i].classList.remove("active");
		}
		indexList[index-1].classList.add("active");

	}
}

//主题内容
function manEffect(){
	// 顶部出现
	var manInfo = document.querySelectorAll(".man li");
	width = 100/manInfo.length;
	for (var i = 0; i < manInfo.length; i++) {
		manInfo[i].style = "width:"+width+"%;";
	}

	$(window).scroll(function() {
		toHeight = $(this).scrollTop()+40;   //页面滚动的高度
		var tab_nav = $(".tab_nav");
        manHight = $(".tab_nav").offset().top;
  		if(toHeight > manHight){
  			if(tab_nav.attr("is_clone") != 1){
  				tab_nav.attr('is_clone','1');
  				var tab_nav_clone = tab_nav.clone(true);
  				tab_nav_clone.addClass("tab_nav_active");
  				tab_nav.append(tab_nav_clone);
  			}

  		}
  		if(toHeight < manHight)
  		{
  			if(tab_nav.attr("is_clone") == 1){
  				$(".tab_nav_active").remove();
  				tab_nav.attr("is_clone",0)
  			}
  		}
	});

	// 主体滑动切换
	var man = document.querySelector(".man");

	man.addEventListener("touchstart",function(e){
		manStartX = e.changedTouches[0].clientX;
	});

	// man.addEventListener("touchmove",function(e){
	// 	manMoveX = e.changedTouches[0].clientX;
	// });

	man.addEventListener("touchend",function(e){
		manEndX = e.changedTouches[0].clientX - manStartX;
		// manEndX = manMoveX-manStartX;
		if(Math.abs(manEndX) > 100){
			ManList = document.querySelectorAll(".tab_nav>li")
			for(var i = 0; i < ManList.length; i++){
				if(ManList[i].classList.contains('active')){
					if(manEndX<0){
						i++;
						if(i>=ManList.length){
							return;
						}
						tab('tab'+(i+1),ManList[i]);
					}else{
						if(i<1){
							return;
						}
						i--;
						tab('tab'+(i+1),ManList[i]);
					}
				}
			}
			
		}
	});
}


function tab(name,e){
	var divInfo = document.querySelectorAll(".man>div");
	var info = document.querySelectorAll(".tab_nav>li");
	var tab_index = $(e).attr("tab");
	for(var i = 0; i < info.length; i++){
		info[i].classList.remove('active');
		if(typeof(divInfo[i]) != 'undefined'){
			divInfo[i].style="display:none";
		}
		if(info[i].getAttribute("tab") == tab_index){
			info[i].classList.add("active");
		}
	}
	document.querySelector('#'+name).style="display:block;";
	$(document).scrollTop($(".tab_nav").offset().top-40); 
}



