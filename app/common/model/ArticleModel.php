<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\common\model;
use think\Db;

/**
 * 分类模型的模型(笑哭)
 */
class ArticleModel extends ModelBase{	

    //检索数据库内所有以model_开头的表
    public function getTableList(){
    	$database_name = config('database.database');	//数据库名称

    	$table_prefix = config('database.prefix');	//表前缀

    	$info = Db::query('
	    		select `table_name`,`table_comment` 
	    		from information_schema.tables where 
	    		`table_name` like "'.$table_prefix.'%" and 
	    		`table_schema` = "'.$database_name.'"
    		');

    	return $info;
    }


}
