<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\common\model;

/**
 * 文章模型
 */
class Article extends ModelBase{
    /**
     * 获取文章列表
     * a文章表 p图片表 m模型表
     */
    public function getLists($where,$field = 'a.*,m.alias as m_alias,m.status as m_status',$continue='0',$page='15'){
		$where = array_merge($where,[
            'a.status'=>1,
        ]);

        $list = db('article')   //读取文章基本信息
        ->field($field)
        ->alias('a')
        ->join('article_model m','a.model_id = m.id')
        ->where($where)
        ->order('a.sort')
        ->limit($continue,$page)
        ->select();

        foreach($list as $k=>$v){

            if($v['cover_id'] > 0){ //缩略图处理
                $list[$k]['path'] = db('picture')->where(['id'=>$v['cover_id']])->value('path');
            }

            //附加字段处理
            if($v['m_status'] == 1){
                $table_name = 'model_' . $v['m_alias'];
                $row = db($table_name)->where(['aid'=>$v['id']])->find();
                if(is_array($row)){
                    foreach($row as $key=>$val){
                        if($key != 'aid'){ $list[$k][$key] = $val; }
                    }
                }
            }
            else{
                unset($list[$k]);
            }
        }
		 
		return $list;
    }

    /**
     * 获取文章列表
     * a文章表 p图片表 m模型表
     * 该方法只读取有缩略图的
     */
    public function getThubLists($where,$field = 'a.*,m.alias as m_alias,m.status as m_status,p.path',$continue='0',$page='15'){
        $where = array_merge($where,[
            'a.status'=>1,
        ]);

        $list = db('article')   //读取文章基本信息
        ->field($field)
        ->alias('a')
        ->join('article_model m','a.model_id = m.id')
        ->join('picture p','a.cover_id = p.id')
        ->where($where)
        ->order('a.sort')
        ->limit($continue,$page)
        ->select();

        foreach($list as $k=>$v){   //获取附加表字段
            if($v['m_status'] == 1){
                $table_name = 'model_' . $v['m_alias'];
                $row = db($table_name)->where(['aid'=>$v['id']])->find();
                if(is_array($row)){
                    foreach($row as $key=>$val){
                        if($key != 'aid'){ $list[$k][$key] = $val; }
                    }
                }
            }else{
                unset($list[$k]);
            }
        }
         
        return $list;
    }
}
