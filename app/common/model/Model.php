<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\common\model;

/**
 * 模型
 */
class Model extends ModelBase
{
    
    /**
     * 获取模型表名
     */
    public function getTable($model_id,$prefix=false)
    {
        $table_name =  db('article_model')->where(['id'=>$model_id])->value('alias');
        if(empty($table_name)) return false;
        if($prefix){
        	return config('database.prefix') . 'model_' . $table_name;
        }
        return 'model_' . $table_name;
    }

}
