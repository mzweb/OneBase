<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\common\logic;

use think\Db;

/**
 * 公共模型
 */
class Common extends LogicBase{
    /**
     * [树状结构排序]
     * @param [type] $list   [数据]
     * @param string $pid    [父id 默认]
     * @param string $level  [级别 默认]
     * @param string $index  [记录主键   可自定义]
     * @param string $parent [父记录主键 可自定义]
     */
    public function ListTree($list,$pid = '0',$level = '0',$index = 'id',$parent = 'pid'){ //列表树状结构
        static $arr = [];
        foreach($list as $k=>$v){
            if($v[$parent] == $pid){
                $v['level'] = $level+1;
                $arr[] = $v;
                $this->ListTree($list,$v[$index],$v['level'],$index,$parent);
            }
        }
        return $arr;
    }

    /**
     * 树状名称改写
     * @param [type] $list [数据]
     * @param string $name [需要改写的字段]
     */
    public function TreeAlias($list,$name = 'name'){
        foreach($list as $k=>$v){
            $list[$k][$name] = str_repeat('&nbsp',$v['level'] * 4). '|-' .$v[$name];
        }
        return $list;
    }


}
