<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\common\logic;

/**
 * 分类模型逻辑
 */
class Model extends LogicBase{

    /**
     * 模型搜索条件
     */
    public function getModelWhere($data){
        $where = [];

        !empty($data['search']) && $where['name']=['like','%' . $data['search'] . '%'];

        return $where;
    }
    
    /**
     * 获取模型列表
     * @return [type] [description]
     */
    public function getModelList($data = []){

        $data = $this->getModelWhere($data);
        
        return $this->modelArticleModel->getList($data,true,'sort',false);
    }

    /**
     * 获取模型信息
     */
    public function getModelInfo($model_id){
        return $this->modelArticleModel->getInfo(['id'=>$model_id]);
    }

    /**
     * 模型删除
     */
    public function modelDel($data = []){
        $name = $this->modelArticleModel->getValue($data,'name');

        $result = $this->modelArticleModel->deleteInfo($data);

        $url = url('modelList');

        $handle_text = '删除';

        $result && action_log($handle_text,'模型' . $handle_text . ', name:' . $name);

        return $result ? [RESULT_SUCCESS, '模型删除成功', $url] : [RESULT_ERROR, $this->modelArticleModel->getError()];
    }

    /**
     * 模型信息处理
     */
    public function modelEdit($data = []){
        
        $validate_result = $this->validateModel->scene('edit')->check($data);
        if (!$validate_result) {
            
            return [RESULT_ERROR, $this->validateModel->getError()];
        }

        $url = url('modelList');
        
        $result = $this->modelArticleModel->setInfo($data);
        
        if(empty($data['val_id'])){ //新增添加模型表
            $table_name = config('database.prefix') . 'model_' . $data['alias'];
            $is_table = db()->query('show tables like "'. $table_name .'"');
            if(empty($is_table)){
                db()->execute("create table `{$table_name}`(
                    `aid` int unsigned primary key
                )engine=innodb default charset=utf8mb4 comment '栏目模型'");   
            }
            $handle_text = '新增';
        }else{
            $handle_text = '编辑';
        }
        
        $result && action_log($handle_text, '模型' . $handle_text . '，name：' . $data['name']);

        return $result !== false ? [RESULT_SUCCESS, '模型操作成功', $url] : [RESULT_ERROR, $this->modelArticleModel->getError()];
    }

    

    /**
     * 模型字段信息处理
     */
    public function modelFieldEdit($data = []){

        $validate = $this->validateModelField;

        $validate_result = $validate->check($data);
        if (!$validate_result) {
            return [RESULT_ERROR, $validate->getError()];
        }

        $url = url('fieldList',['model_id'=>$data['model_id']]);

        isset($data['value']) && $data['value'] = str_replace('，',',',$data['value']);

        $result = $this->modelArticleModelVal->setInfo($data);

        if(empty($data['val_id'])){ //新增
            $table_name = db('article_model')->where(['id'=>$data['model_id']])->value('alias');
            $table_name = config('database.prefix').'model_'.$table_name;
            // 1:文本; 2:单选; 3:复选; 4:下拉; 5:文本域; 6:附件; 7:时间戳;
            switch($data['type']){
                case 1:
                    $data_type = 'varchar(255)';
                    break;
                case 2:
                    $data_type = 'varchar(20)';
                    break;
                case 3:
                    $data_type = 'varchar(200)';
                    break;
                case 4:
                    $data_type = 'tinyint unsigned';
                    break;
                case 5:
                    $data_type = 'varchar(100)';
                    break;
                case 6:
                    $data_type = 'varchar(200)';
                    break;
                default:
                    $data_type = 'int unsigned';
            }
            db()->query("alter table `{$table_name}` add `{$data['name_en']}` {$data_type} ");
            $handle_text = '新增';
        }else{
            $handle_text = '编辑';
        }
        
        $result && action_log($handle_text, '模型字段' . $handle_text . '，name：' . $data['name_cn']);

        return $result ? [RESULT_SUCCESS, '模型字段操作成功', $url] : [RESULT_ERROR, $this->validate->getError()];

    }

    /**
     * 模型字段列表获取
     */
    public function getFieldList($model_id,$where = []){

        $where = array_merge($where,['model_id'=>$model_id]);

        return $this->modelArticleModelVal->getList($where,true,'sort',false);
    }

    /**
     * 模型字段信息获取
     */
    public function getFieldInfo($val_id){
        return $this->modelArticleModelVal->getInfo(['val_id'=>$val_id]);
    }

    /**
     * 模型字段删除
     */
    public function delField($param){
        foreach($param['ids'] as $k=>$v){
            $field_info = db('article_model_val')->where(['val_id'=>$v['value']])->find();
            $table_name = db('article_model')->where(['id'=>$field_info['model_id']])->value('alias');
            $table_name = config('database.prefix').'model_'.$table_name;
            db()->query("alter table `{$table_name}` drop column `{$field_info['name_en']}`");
            $this->modelArticleModelVal->deleteInfo(['val_id'=>$field_info['val_id']],true);
        }
        $url = '';
        return [RESULT_SUCCESS, '模型字段操作成功', $REQUEST_SCHEME .'://'. $_SERVER['HTTP_REFERER']];
    }










/////////
// out //
/////////


    /**
     * 文章分类编辑
     */
    public function articleCategoryEdit($data = [])
    {
        
        $validate_result = $this->validateArticleCategory->scene('edit')->check($data);
        
        if (!$validate_result) {
            
            return [RESULT_ERROR, $this->validateArticleCategory->getError()];
        }
        
        $url = url('articleCategoryList');
        
        $result = $this->modelArticleCategory->setInfo($data);
        
        $handle_text = empty($data['id']) ? '新增' : '编辑';
        
        $result && action_log($handle_text, '文章分类' . $handle_text . '，name：' . $data['name']);
        
        return $result ? [RESULT_SUCCESS, '操作成功', $url] : [RESULT_ERROR, $this->modelArticleCategory->getError()];
    }
    
    /**
     * 获取文章列表
     */
    public function getArticleList($where = [], $field = 'a.*,m.nickname,c.name as category_name', $order = '')
    {
        
        $this->modelArticle->alias('a');
        
        $join = [
                    [SYS_DB_PREFIX . 'member m', 'a.member_id = m.id'],
                    [SYS_DB_PREFIX . 'article_category c', 'a.category_id = c.id'],
                ];
        
        $where['a.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        
        $this->modelArticle->join = $join;
        
        return $this->modelArticle->getList($where, $field, $order);
    }
    
    /**
     * 获取文章列表搜索条件
     */
    public function getWhere($data = [])
    {
        
        $where = [];
        
        !empty($data['search_data']) && $where['a.name|a.describe'] = ['like', '%'.$data['search_data'].'%'];
        
        return $where;
    }
    
    /**
     * 文章信息编辑
     */
    public function articleEdit($data = [])
    {
        
        $validate_result = $this->validateArticle->scene('edit')->check($data);
        
        if (!$validate_result) {
            
            return [RESULT_ERROR, $this->validateArticle->getError()];
        }
        
        $url = url('articleList');
        
        empty($data['id']) && $data['member_id'] = MEMBER_ID;
        
        $data['content'] = html_entity_decode($data['content']);
        
        $result = $this->modelArticle->setInfo($data);
        
        $handle_text = empty($data['id']) ? '新增' : '编辑';
        
        $result && action_log($handle_text, '文章' . $handle_text . '，name：' . $data['name']);
        
        return $result ? [RESULT_SUCCESS, '文章操作成功', $url] : [RESULT_ERROR, $this->modelArticle->getError()];
    }

    /**
     * 获取文章信息
     */
    public function getArticleInfo($where = [], $field = 'a.*,m.nickname,c.name as category_name')
    {
        
        $this->modelArticle->alias('a');
        
        $join = [
                    [SYS_DB_PREFIX . 'member m', 'a.member_id = m.id'],
                    [SYS_DB_PREFIX . 'article_category c', 'a.category_id = c.id'],
                ];
        
        $where['a.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        
        $this->modelArticle->join = $join;
        
        return $this->modelArticle->getInfo($where, $field);
    }
    
    /**
     * 获取分类信息
     */
    public function getArticleCategoryInfo($where = [], $field = true)
    {
        
        return $this->modelArticleCategory->getInfo($where, $field);
    }
    
    /**
     * 获取文章分类列表
     */
    public function getArticleCategoryList($where = [], $field = true, $order = '', $paginate = 0)
    {
        
        return $this->modelArticleCategory->getList($where, $field, $order, $paginate);
    }
    
    /**
     * 文章分类删除
     */
    public function articleCategoryDel($where = [])
    {
        
        $result = $this->modelArticleCategory->deleteInfo($where);
        
        $result && action_log('删除', '文章分类删除，where：' . http_build_query($where));
        
        return $result ? [RESULT_SUCCESS, '文章分类删除成功'] : [RESULT_ERROR, $this->modelArticleCategory->getError()];
    }
    
    /**
     * 文章删除
     */
    public function articleDel($where = [])
    {
        
        $result = $this->modelArticle->deleteInfo($where);
        
        $result && action_log('删除', '文章删除，where：' . http_build_query($where));
        
        return $result ? [RESULT_SUCCESS, '文章删除成功'] : [RESULT_ERROR, $this->modelArticle->getError()];
    }
}
