<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\api\controller;

/**
 * 用户接口控制器
 */
class Member extends ApiBase{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 用户基础信息接口
     */
    public function getInfos(){
        return $this->apiReturn($this->logicMember->getInfos($this->param['id']));
    }
    
    /**
     * 文章分类接口
     */
    public function categoryList()
    {
        
        return $this->apiReturn($this->logicArticle->getArticleCategoryList());
    }
    
}
