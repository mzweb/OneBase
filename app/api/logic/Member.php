<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\api\logic;


/**
 * 文章接口逻辑
 */
class Member extends ApiBase
{
    
    
    /**
     * 基类初始化
     */
    public function __construct()
    {
        // 执行父类构造方法
        parent::__construct();
        
    }
    
    /**
     * 获取用户相关信息
     */
    public function getInfos($member_id)
    {
        if(!isset($member_id)) return $this->apiError('用户id不能为空');
        $where = [];
        return db(CONTROLLER_NAME)->find($member_id);
        
    }
}
