<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\api\logic;

use app\common\logic\Article as CommonArticle;
use app\common\model\Article as CommmonModelArticle;
use app\index\logic\IndexBase;

/**
 * 文章接口逻辑
 */
class Article extends ApiBase
{
    
    public static $commonArticleLogic = null;
    
    /**
     * 基类初始化
     */
    public function __construct()
    {
        // 执行父类构造方法
        parent::__construct();
        
        empty(static::$commonArticleLogic) && static::$commonArticleLogic = get_sington_object('Article', CommonArticle::class);
    }

    
    
    /**
     * 获取文章分类列表
     */
    public function getArticleCategoryList()
    {
        
        return static::$commonArticleLogic->getArticleCategoryList([], 'id,name', 'id desc', false);
    }
    
    /**
     * 获取文章列表
     */
    public function getArticleList($data = [])
    {
        $IndexBase = new IndexBase();
        $id = $IndexBase->aliasGetId('letter');
        $model = new CommmonModelArticle();
        $list = $model->getLists([
            'a.category_id'=>$id
        ],'a.*,m.alias as m_alias,m.status as m_status',input('continues'),input('page'));
        return $list;
    }
    
    /**
     * 获取文章信息
     */
    public function getArticleInfo($data = [])
    {
        
        return static::$commonArticleLogic->getArticleInfo(['a.id' => $data['article_id']], 'a.*,m.nickname,c.name as category_name');
    }
}
